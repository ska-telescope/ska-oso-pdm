#
# CAR_OCI_REGISTRY_HOST, CAR_OCI_REGISTRY_USERNAME and PROJECT_NAME are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST (=artefact.skao.int) and overwrites
# PROJECT to give a final Docker tag of artefact.skao.int/ska-oso-pdm
#
CAR_OCI_REGISTRY_HOST ?= artefact.skao.int
CAR_OCI_REGISTRY_USERNAME ?= ska-telescope
PROJECT_NAME = ska-oso-pdm

# Set sphinx documentation build to fail on warnings (as it is configured
# in .readthedocs.yaml as well)
DOCS_SPHINXOPTS ?= -W --keep-going

OCI_IMAGE_BUILD_CONTEXT = $(PWD)

# include makefile to pick up the standard Make targets from the submodule
-include .make/base.mk
-include .make/python.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

# Unset Black and isort configs so that definitions in pyproject.toml take effect
PYTHON_LINE_LENGTH = 88
GENERATE_OPENAPI_YAML = poetry run python src/ska_oso_pdm/openapi/generate_specs.py

# https://import-linter.readthedocs.io/
# Enforce project architecture.
python-pre-lint:
	lint-imports

# Static type-checking
# https://microsoft.github.io/pyright/
python-post-lint:
	pyright src/

# run openapi to update yaml files with latest pdm_version
python-post-set-release:
	poetry install
	$(GENERATE_OPENAPI_YAML)

plantuml:  ## recreate PlantUML diagrams whose source has been modified
	@for file in $$(git diff --staged --name-only -- '*.puml'; git diff --name-only -- '*.puml'); \
	do \
		if [ -e "$$file" ]; then \
			echo "Processing $$file"; \
			cat $$file | docker run --rm -i think/plantuml -tsvg $$file > $${file%.puml}.svg; \
		fi; \
	done


PUML_FILES := $(shell find . -type f -name '*.puml')
plantuml-all:  ## recreate all PlantUML diagrams, regardless of whether they have been modified
	@for file in $(PUML_FILES); \
	do \
		if [ -e "$$file" ]; then \
			echo "Processing $$file"; \
			cat $$file | docker run --rm -i think/plantuml -tsvg $$file > $${file%.puml}.svg; \
		fi; \
	done

py2puml:  ## Generate PlantUML files from project code (requires forked PlantUML, see SJW)
	# work around some py2puml bugs
	echo "from typing import Union" >> src/ska_oso_pdm/sb_definition/sb_definition.py
	echo "import astropy.units.core" >> src/ska_oso_pdm/_shared/custom_types.py
	echo "from typing import Union" >> src/ska_oso_pdm/_shared/custom_types.py
	echo "import pydantic_core" >> src/ska_oso_pdm/_shared/custom_types.py
	echo "import astropy.units.quantity" >> src/ska_oso_pdm/_shared/target.py
	echo "from .procedures import InlineScript" >> src/ska_oso_pdm/sb_definition/sb_definition.py
	echo "from datetime import timedelta" >> src/ska_oso_pdm/sb_definition/sb_definition.py
	echo "from datetime import timedelta" >> src/ska_oso_pdm/sb_definition/scan_definition.py
	echo "from datetime import timedelta" >> src/ska_oso_pdm/sb_definition/csp/lowcbf.py

	py2puml src/ska_oso_pdm ska_oso_pdm > docs/src/uml/ska_oso_pdm.puml
	py2puml src/ska_oso_pdm/entity_status_history.py ska_oso_pdm.entity_status_history > docs/src/uml/ska_oso_pdm_entity_status_history.puml
	py2puml src/ska_oso_pdm/execution_block.py ska_oso_pdm.execution_block > docs/src/uml/ska_oso_pdm_execution_block.puml
	py2puml src/ska_oso_pdm/project.py ska_oso_pdm.project > docs/src/uml/ska_oso_pdm_project.puml
	py2puml src/ska_oso_pdm/proposal.py ska_oso_pdm.proposal > docs/src/uml/ska_oso_pdm_proposal.puml
	py2puml src/ska_oso_pdm/sb_definition ska_oso_pdm.sb_definition > docs/src/uml/ska_oso_pdm_sb_definition.puml
	py2puml src/ska_oso_pdm/sb_definition/_shared ska_oso_pdm._shared > docs/src/uml/ska_oso_pdm__shared.puml
	py2puml src/ska_oso_pdm/sb_definition/_shared/target.py ska_oso_pdm._shared.target > docs/src/uml/ska_oso_pdm__shared_target.puml
	py2puml src/ska_oso_pdm/sb_definition/csp ska_oso_pdm.sb_definition.csp > docs/src/uml/ska_oso_pdm_sb_definition_csp.puml
	py2puml src/ska_oso_pdm/sb_definition/dish/dish_allocation.py ska_oso_pdm.sb_definition.dish.dish_allocation > docs/src/uml/ska_oso_pdm_sb_definition_dish_dish_allocation.puml
	py2puml src/ska_oso_pdm/sb_definition/mccs/mccs_allocation.py ska_oso_pdm.sb_definition.mccs.mccs_allocation > docs/src/uml/ska_oso_pdm_sb_definition_mccs_mccs_allocation.puml
	py2puml src/ska_oso_pdm/sb_definition/procedures.py ska_oso_pdm.sb_definition.procedures > docs/src/uml/ska_oso_pdm_sb_definition_procedures.puml
	py2puml src/ska_oso_pdm/sb_definition/scan_definition.py ska_oso_pdm.sb_definition.scan_definition > docs/src/uml/ska_oso_pdm_sb_definition_scan_definition.puml
	py2puml src/ska_oso_pdm/sb_definition/sdp ska_oso_pdm.sb_definition.sdp > docs/src/uml/ska_oso_pdm_sb_definition_sdp.puml
	py2puml src/ska_oso_pdm/sb_instance.py ska_oso_pdm.sb_instance > docs/src/uml/ska_oso_pdm_sb_instance.puml

	# and restore...
	sed -i '$$d' src/ska_oso_pdm/sb_definition/csp/lowcbf.py
	sed -i '$$d' src/ska_oso_pdm/sb_definition/scan_definition.py
	sed -i '$$d' src/ska_oso_pdm/sb_definition/sb_definition.py
	sed -i '$$d' src/ska_oso_pdm/sb_definition/sb_definition.py
	sed -i '$$d' src/ska_oso_pdm/sb_definition/sb_definition.py
	sed -i '$$d' src/ska_oso_pdm/_shared/custom_types.py
	sed -i '$$d' src/ska_oso_pdm/_shared/custom_types.py
	sed -i '$$d' src/ska_oso_pdm/_shared/custom_types.py
	sed -i '$$d' src/ska_oso_pdm/_shared/target.py


pyreverse:  ## use pyreverse to generate PlantUML
	@while IFS= read -r line; do \
		if [ "$${line:0:1}" != "#" ]; then \
			INPUTS=$$(echo $$line | awk 'BEGIN{OFS=" "}{$$NF=""; print $0}'); \
			OUTPUT=$$(echo $$line | awk '{print $$NF}'); \
			if [ -n "$$INPUTS" ]; then \
				echo -n "Updating $$OUTPUT... "; \
				pyreverse \
					-ASmy \
					-o puml \
					--source-roots ~/ska/src/ska-oso-pdm/src \
					-d ./docs/src/uml \
					$$INPUTS; \
				sed -i -f ./docs/src/uml/patterns ./docs/src/uml/classes.puml; \
				mv ./docs/src/uml/classes.puml ./docs/src/uml/$$OUTPUT; \
			fi; \
		fi; \
	done < ./docs/src/uml/pyreverse_cmds

# MINIKUBE_NFS_SHARE_PATH is appended to the volume mounted during code generation.
# Developers can redefine this value to match their Minikube NFS share setup,
# which will allow the codegen For instance, a Mac where SKA minikube is installed
# with a command like
#
#    make RUNTIME=docker --nfs-share=$HOME --nfs-shares-root=/nfsshares minikube-install
#
# the $HOME directory would be mounted at /nfsshares, so MINIKUBE_NFS_SHARE_PATH should
# also be set to /nfsshares
MINIKUBE_NFS_SHARES_ROOT ?=

openapi:
	$(GENERATE_OPENAPI_YAML)

unified:
	$(OCI_BUILDER) run --rm --volume "$(MINIKUBE_NFS_SHARES_ROOT)$(PWD):/local" openapitools/openapi-generator-cli \
    	generate \
		-i /local/src/ska_oso_pdm/openapi/oso-components-openapi-v1.yaml \
    	-g openapi-yaml \
		-o /local/openapi \
		--additional-properties outputFile=/local/openapi/unified.yaml

# The docs build fails unless the project and dependencies are installed locally as our Pydantic workarounds require it
docs-pre-build:
	poetry install --with docs
