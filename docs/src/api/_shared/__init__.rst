.. _api-_shared:

*******************
ska_oso_pdm._shared
*******************

The ska_oso_pdm._shared package model entities and types that are
used by more than one OSO entity, for example the Target that is used by both
SB Definitions and Observing Proposals. Package contents are presented in the
diagram below.

.. figure:: ../../uml/ska_oso_pdm__shared.svg
   :align: center

   Class diagram for the ska_oso_pdm._shared package

.. automodule:: ska_oso_pdm._shared.pdm_object
    :members:

.. automodule:: ska_oso_pdm._shared.python_arguments
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm._shared.metadata
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm._shared.atoms
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
