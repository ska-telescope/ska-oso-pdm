.. _api-execution_blocks:

***************************
ska_oso_pdm.execution_block
***************************

SKA needs a way to link data to the observations that create the data. To summarise the purpose of Execution Blocks (EBs),
EBs exist to link SDP data to the observing session that created the data, be it an observation driven by the OET using an
SB Instance or an interactive session where control was exerted via other interface.

The Execution Block defined in the PDM is used to record references to the OSO entities whose execution created the data
(Scheduling Block Instances, Scheduling Block Definitions, etc.), the sequence of commands and responses that led to the data,
and to link to the SBD Status entity, which is the OSO entity that defines the current state of the SB Definition within the SB Definition lifecycle.

.. figure:: ../uml/ska_oso_pdm_execution_block.svg
   :align: center

   Class diagram for an Execution Block


A current example of the Execution Block json, with examples of handling success and error responses from commands, can be seen below:

.. literalinclude:: ../../../tests/unit/ska_oso_pdm/serialisation_cases/testfile_sample_execution_block.json
    :language: JSON

.. automodule:: ska_oso_pdm.execution_block
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
