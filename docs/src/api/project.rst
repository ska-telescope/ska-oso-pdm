.. _api-project:

*******************
ska_oso_pdm.project
*******************

.. figure:: ../uml/ska_oso_pdm_project.svg
   :align: center

   Class diagram for an OSO Project


A current example of Project json is presented below:

.. literalinclude:: ../../../tests/unit/ska_oso_pdm/serialisation_cases/testfile_sample_project.json
    :language: JSON

.. automodule:: ska_oso_pdm.project
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
