.. _api-proposal:

********************
ska_oso_pdm.proposal
********************

.. figure:: ../uml/ska_oso_pdm_proposal.svg
   :align: center

   Class diagram for an OSO Proposal


A current example of Proposal json is presented below:

.. literalinclude:: ../../../tests/unit/ska_oso_pdm/serialisation_cases/proposal/testfile_sample_proposal_submitted.json
    :language: JSON

.. automodule:: ska_oso_pdm.proposal
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
