.. _api-sb_definition_dish:

******************************
ska_oso_pdm.sb_definition.dish
******************************

The ska_oso_pdm.sb_definition.dish package contains classes used to define
resource allocations and configurations for SKA MID dishes.

==============================================
ska_oso_pdm.sb_definition.dish.dish_allocation
==============================================

The dish_allocation module defines which SKA MID dishes should be allocated to
a sub-array prior to an observation.

.. figure:: ../../../uml/ska_oso_pdm_sb_definition_dish_dish_allocation.svg
   :align: center

   Class diagram for the dish_allocation module

An example serialisation of this model to JSON is shown below.

.. literalinclude:: ../../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :start-at: "dish_allocations": {

.. automodule:: ska_oso_pdm.sb_definition.dish.dish_allocation
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
