.. _api-sb_definition_mccs:

******************************
ska_oso_pdm.sb_definition.mccs
******************************

The ska_oso_pdm.sb_definition.mccs package contains classes used to define
MCCS resource allocations and configurations.

==============================================
ska_oso_pdm.sb_definition.mccs.mccs_allocation
==============================================

The ska_oso_pdm.sb_definition.mccs.mccs_allocation module defines which SKA
LOW stations should be allocated to a sub-array beam prior to an observation.

.. figure:: ../../../uml/ska_oso_pdm_sb_definition_mccs_mccs_allocation.svg
   :align: center

   Class diagram for the mccs_allocation module

An example serialisation of this model to JSON is shown below.

.. literalinclude:: ../../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_low_sb.json
    :language: JSON
    :start-at: "mccs_allocation": {
    :end-before: "csp_configurations": [

.. automodule:: ska_oso_pdm.sb_definition.mccs.mccs_allocation
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units


