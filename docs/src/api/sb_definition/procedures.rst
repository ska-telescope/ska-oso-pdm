.. _api-sb_definition_procedures:

************************************
ska_oso_pdm.sb_definition.procedures
************************************

The ska_oso_pdm.sb_definition.procedures module models SB entities concerned
with the script execution requirements. This includes the scripts that should
process the SB and the git identifier, including the repository and branch.

.. figure:: ../../uml/ska_oso_pdm_sb_definition_procedures.svg
   :align: center

   Class diagram for the procedure module

An example serialisation of this model to JSON is shown below.

.. literalinclude:: ../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :language: JSON
    :start-at: "activities": {
    :end-before: "scan_definitions": [

.. automodule:: ska_oso_pdm.sb_definition.procedures
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
