.. _api-sb_definition_sb_definition:

***************************************
ska_oso_pdm.sb_definition.sb_definition
***************************************

The ska_oso_pdm.sb_definition.sb_definition module models SB data model
entities concerned with the high-level composition of a Scheduling Block. An
SB defines everything needed to schedule and perform an observation, for
instance:

* Target telescope;
* Targets and field positions, describing which points to observe on the sky;
* Dish allocations to sub-arrays (for SKA MID);
* Dish configurations (receiver bands, etc., for SKA MID);
* MCCS resource allocations to sub-arrays (for SKA LOW);
* CSP (Central Signal Processing) correlator configurations to be used;
* SDP configuration, defining the required pipeline workflows for the
  observation;
* Scan information, which describes which CSP/SDP/dish/target configurations
  to be used for each scan;
* Scan sequence describing the sequence of scans constituting the observation.

as well as:

* SB metadata (author, creation date and version number, as well as edit history);
* SB script execution requirements;

The contents of the module are presented in the diagram below.


.. figure:: sb_definition.svg
   :align: center

   Class diagram for the sb_definition module

.. literalinclude:: ../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :language: JSON

.. automodule:: ska_oso_pdm.sb_definition.sb_definition
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
