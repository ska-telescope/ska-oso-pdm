.. _api-sb_definition_sdp:

*****************************
ska_oso_pdm.sb_definition.sdp
*****************************

The ska_oso_pdm.sb_definition.sdp package contains modules that model SB
entities concerned with SDP resource allocation and pipeline workflow
configuration. The contents of the package are presented in the diagram below.

.. figure:: ../../../uml/ska_oso_pdm_sb_definition_sdp.svg
   :align: center

   Class diagram for the sdp package

An example serialisation of this model to JSON is shown below.

.. literalinclude:: ../../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :language: JSON
    :start-at: "sdp_configuration": {
    :end-before: "csp_configurations": [

.. automodule:: ska_oso_pdm.sb_definition.sdp.sdp_configuration
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.execution_block
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.processing_block
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.resources
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.beam
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.channels
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.scan_type
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.sdp.polarisation
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
