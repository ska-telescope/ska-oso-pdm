.. _api-sb_instance:

***********************
ska_oso_pdm.sb_instance
***********************

.. figure:: ../uml/ska_oso_pdm_sb_instance.svg
   :align: center

   Class diagram for an SB Instance


A current example of SB Instance JSON is presented below:

.. literalinclude:: ../../../tests/unit/ska_oso_pdm/serialisation_cases/testfile_sample_sbi.json
    :language: JSON

.. automodule:: ska_oso_pdm.sb_instance
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units
