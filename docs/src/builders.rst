.. _`builders`:

**********************
Generating Example SBs
**********************

Introduction
============

The PDM library includes a module called ``builders`` which provides functions for
generating basic Scheduling Block Definition PDM objects. If this is imported into
a Python environment, it gives the user access to two functions that can create
relatively simple Scheduling Block Definitions (SBDs) for SKA-Mid (``mid_imaging_sb``) and
SKA-Low (``low_imaging_sb``).

If these functions are run without any arguments, an SBD will be created that is complete 
and valid in the sense that it should be possible to perform a basic imaging observation with
it. This is done by assuming the following defaults.

Low
---

* Array: the four stations corresponding to Phase 1 of AA0.5
* Source: 47 Tuc (00:24:05.359, -72:04:53.20)
* Correlator: one spectral window (spw) with a central freqency close to 200 MHz and a bandwidth of 75 MHz
* Scan sequence: one 60-s scan

Mid
---

* Array: the 4 AA0.5 dishes
* Source: SGRS J0400-8456 (04:01:18.4, -84:56:35.9)
* Correlator: one spw with a bandwidth of 268.8 MHz (20,000 channels) and a central frequency of 800 MHz (Band 1)
* Scan sequence: one 60-s scan

In both cases, the spws correspond to the standard-correlation modes (non-zoom) that will be used
in AA0.5.

Changing the defaults
=====================

If desired, it is possible to change all these defaults via arguments to the function call.
In the following example, we show how for a SKA-Low SBD we define two targets using ICRS
coordinates. The resulting SBD will include one scan of each using the default duration of 60s.
Note that it is necessary to import the ``ICRSObject`` class in order to define a target of this
type.

.. code-block:: console

    # Imports
    $ from ska_oso_pdm import builders
    $ from ska_oso_pdm.builders import ICRSObject

    # Define the targets
    $ target1 = ICRSObject(name="PKS J0408-6545", ra="04:08:20.37884", dec="-65:45:09.0806")
    $ target2 = ICRSObject(name="47 Tuc", ra="00:24:05.359", dec="-72:04:53.20")
    # Run the builder with the target definitions given as arguments
    $ sbd = builders.low_imaging_sb(stations=[345,350],targets=[target1,target2])

In a second example, we modify a SKA-Mid SBD to observe a solar-system object (by importing
the ``SolarSystemObject`` class) for longer than the default scan duration. Solar-system
objects recognised by the telescope control software are the seven planets (excluding the
Earth!), the Sun and the Moon.

Note that scan durations are given in **milliseconds**! Further note that even a single
target has to be placed in a list - this is not the case for a single scan duration which
will be applied to all targets.

.. code-block:: console

    # Imports
    $ from ska_oso_pdm import builders
    $ from ska_oso_pdm.builders import SolarSystemObject

    # Define the solar-system target
    $ target = SolarSystemObject(name="Mars")
    # Run the builder with the target definition and scan duration given as arguments
    $ sbd = builders.mid_imaging_sb(targets=[target],scan_duration=600e3)

Further information
===================

See the API documentation :class:`ska_oso_pdm.builders` for the full list of available functions.
A further target class exists for performing five-point calibration scans using a target defined
using ICRS coordinates (``ICRSObjectFivePoint``) but this is not currently supported.