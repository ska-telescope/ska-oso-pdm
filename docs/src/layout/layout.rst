==============
Project layout
==============

.. warning::
    This page is out of date and references the old architecture of
    entity classes and separate Marshmallow schema. This page will be
    updated to reflect the new architecture which makes use of Pydantic.
    Once complete, this warning will be removed.

The PDM project contains one top-level package, ``ska_oso_pdm.entities``
The ``ska_oso_pdm.entities`` package contains Python object models for the
logical entities in the Project Data Model (PDM). These classes are Pydantic
data models that inherit from ``PdmEntity``.

.. figure:: layout.svg
   :align: center

   Project layout and naming conventions.

The project layout and naming conventions are:

* The package ``ska_oso_pdm.entities.shared`` is the lowest-level of
  the data model and contains basic components that are imported and re-used
  throughout the project.
* The SB data model is divided into logical groups that collect
  related entities together.
* For each SB entity grouping, a corresponding Python module is
  created in ``ska_oso_pdm.entities``. If the
  grouping is complex and contains many entities, the modules may
  be located inside a dedicated package for the group.
* The package ``ska_oso_pdm.entities.common`` is the highest-level of the
  the data model and contains complex entities that are common between
  both MID and LOW telescopes.

Entities overview
-----------------

.. figure:: entities.svg
   :align: center

   Overview of PDM entities

The Python object model for the entities defined in the SB data model are
located in the ``ska_oso_pdm.entities`` package. In general, each SB entity is
represented as a Python class and each entity attribute presented as a class
attribute or property.

PDM attributes can be typed as plain Python data types (strings, floats, etc.)
or, where appropriate, represented by rich objects if this provides additional
value to the client. For example, while astronomical coordinates are
represented by floats and strings in the JSON schema, in the object model they
are defined as Astropy
`SkyCoord <https://docs.astropy.org/en/stable/api/astropy.coordinates.SkyCoord.html>`_
instances to ensure ensure correct coordinate handling and permit easier
manipulation downstream. Similarly, quantities with units could be defined as
instances of Astropy
`Quantity <https://docs.astropy.org/en/stable/units/quantity.html>`_ to
provide additional functionality.

For details on the entities modelled by this library, see the pages within
the API documentation.
