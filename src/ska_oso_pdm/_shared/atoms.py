"""
Contains very basic building blocks used elsewhere in the PDM.
Enums, constants, type definitions, etc.
Don't put any fancy models or classes in here.
"""

from enum import Enum


# If we upgrade to Py>3.10, could we use StrEnum?
class TerseStrEnum(str, Enum):
    """Subclass to give enums a terse repr()"""

    def __str__(self):
        return self.value

    def __repr__(self):
        return f"{self.__class__.__name__}.{self.name}"


class TelescopeType(TerseStrEnum):
    SKA_MID = "ska_mid"
    SKA_LOW = "ska_low"
    MEERKAT = "MeerKAT"


class SubArrayLOW(TerseStrEnum):
    AA05_ALL = "AA0.5"
    AA1_ALL = "AA1"
    AA2_ALL = "AA2"
    AA2_CORE_ONLY = "AA2 (core only)"
    AASTAR_ALL = "AA*"
    AASTAR_CORE_ONLY = "AA* (core only)"
    AA4_ALL = "AA4"
    AA4_CORE_ONLY = "AA4 (core only)"
    CUSTOM = "Custom"


class SubArrayMID(TerseStrEnum):
    AA05_ALL = "AA0.5"
    AA1_ALL = "AA1"
    AA2_ALL = "AA2"
    AASTAR_ALL = "AA*"
    AASTAR_SKA_ONLY = "AA* (15m antennas only)"
    AA4_ALL = "AA4"
    AA4_MEERKAT_ONLY = "AA*/AA4 (13.5m antennas only)"
    AA4_SKA_ONLY = "AA4 (15m antennas only)"
