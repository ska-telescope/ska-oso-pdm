import warnings
from datetime import datetime, timezone
from importlib.metadata import version
from typing import Optional

from packaging import version as convertVersion
from pydantic import AwareDatetime, Field, field_validator

from ska_oso_pdm._shared.pdm_object import PdmObject

PDM_VERSION: str = version("ska-oso-pdm")


class Metadata(PdmObject):
    """Represents metadata about other entities."""

    version: int = 1
    created_by: Optional[str] = None
    created_on: AwareDatetime = Field(
        default_factory=lambda: datetime.now(timezone.utc)
    )
    last_modified_by: Optional[str] = None
    last_modified_on: AwareDatetime = Field(
        default_factory=lambda: datetime.now(timezone.utc)
    )
    pdm_version: str = Field(
        title="PDM version",
        default=PDM_VERSION,
        pattern=r"^\d+\.\d+\.\d+[-\w]*$",
        description="The version of the PDM used to generate the entity",
    )

    @field_validator("pdm_version")
    @classmethod
    def pdm_version_is_valid_value(cls, check_version):
        if convertVersion.parse(PDM_VERSION) > convertVersion.parse(check_version):
            warnings.warn(
                f"Object generated with PDM version {check_version}, "
                f"version {PDM_VERSION} in use.",
                DeprecationWarning,
                stacklevel=2,
            )
        return check_version
