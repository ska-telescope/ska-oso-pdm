from typing import Optional

from pydantic import Field

from .pdm_object import PdmObject


class PythonArguments(PdmObject):
    """Represents the arguments for a Python callable."""

    args: list = Field(default_factory=list)
    kwargs: dict = Field(default_factory=dict)


# Do we need this? FunctionArgs seems redundant. ~bjmc 2024-02-20
class FunctionArgs(PdmObject):
    """Represents a Python function & its arguments"""

    function_name: Optional[str] = None
    function_args: Optional[PythonArguments] = None
