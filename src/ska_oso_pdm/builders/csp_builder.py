import functools
from datetime import timedelta

from ska_oso_pdm.builders.utils import csp_configuration_id
from ska_oso_pdm.sb_definition import CSPConfiguration
from ska_oso_pdm.sb_definition.csp import (
    CommonConfiguration,
    LowCBFConfiguration,
    MidCBFConfiguration,
)
from ska_oso_pdm.sb_definition.csp.lowcbf import Correlation
from ska_oso_pdm.sb_definition.csp.midcbf import (
    ChannelAveragingFactor,
    CorrelationSPWConfiguration,
    ReceiverBand,
    Subband,
)

CSPConfigurationBuilder = functools.partial(
    CSPConfiguration, config_id=csp_configuration_id(), name="csp config 1"
)

MidCSPConfigurationBuilder = functools.partial(
    CSPConfigurationBuilder,
    common=CommonConfiguration(subarray_id=1),
    midcbf=MidCBFConfiguration(
        frequency_band=ReceiverBand.BAND_2,
        subbands=[
            Subband(
                correlation_spws=[
                    CorrelationSPWConfiguration(
                        spw_id=1,
                        logical_fsp_ids=[1],
                        zoom_factor=0,
                        centre_frequency=450007040.0,
                        number_of_channels=14880,
                        channel_averaging_factor=ChannelAveragingFactor.ONE,
                        time_integration_factor=1,
                    )
                ]
            )
        ],
    ),
)

LowCSPConfigurationBuilder = functools.partial(
    CSPConfigurationBuilder,
    lowcbf=LowCBFConfiguration(
        correlation_spws=[
            Correlation(
                spw_id=1,
                logical_fsp_ids=[0, 1],
                zoom_factor=0,
                centre_frequency=199.609375e6,
                number_of_channels=96,
                integration_time_ms=timedelta(milliseconds=849),
            )
        ]
    ),
)
