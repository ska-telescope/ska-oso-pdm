import functools

from ska_oso_pdm import Metadata, TelescopeType
from ska_oso_pdm.builders.csp_builder import (
    LowCSPConfigurationBuilder,
    MidCSPConfigurationBuilder,
)
from ska_oso_pdm.builders.dish_builder import DishAllocationBuilder
from ska_oso_pdm.builders.mccs_builder import MCCSAllocationBuilder
from ska_oso_pdm.builders.target_builder import LowTargetBuilder, MidTargetBuilder
from ska_oso_pdm.sb_definition import PythonArguments, SBDefinition
from ska_oso_pdm.sb_definition.procedures import GitScript

_SBDefinitionBuilder = functools.partial(
    SBDefinition,
    metadata=Metadata(created_by="TestUser", last_modified_by="TestUser"),
    activities={
        "observe": GitScript(
            path="git://scripts/allocate_and_observe_sb.py",
            repo="https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git",
            branch="master",
            function_args={"init": PythonArguments(kwargs={"subarray_id": 1})},
        )
    },
)

LowSBDefinitionBuilder = functools.partial(
    _SBDefinitionBuilder,
    telescope=TelescopeType.SKA_LOW,
    mccs_allocation=MCCSAllocationBuilder(),
    targets=[LowTargetBuilder()],
    csp_configurations=[LowCSPConfigurationBuilder()],
)

MidSBDefinitionBuilder = functools.partial(
    _SBDefinitionBuilder,
    telescope=TelescopeType.SKA_MID,
    dish_allocations=DishAllocationBuilder(),
    targets=[MidTargetBuilder()],
    csp_configurations=[MidCSPConfigurationBuilder()],
)
