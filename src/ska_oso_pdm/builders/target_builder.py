import functools

from ska_oso_pdm.builders.utils import target_id
from ska_oso_pdm.sb_definition import EquatorialCoordinates, Target

MidTargetBuilder = functools.partial(
    Target,
    target_id=target_id(),
    name="SGRS J04008456",
    reference_coordinate=EquatorialCoordinates(ra="04:01:18.4", dec="-84:56:35.9"),
)

LowTargetBuilder = functools.partial(
    Target,
    target_id=target_id(),
    name="47 Tuc",
    reference_coordinate=EquatorialCoordinates(ra="00:24:05.359", dec="-72:04:53.20"),
)
