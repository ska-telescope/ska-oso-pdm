from os import PathLike
from typing import Type

from ska_oso_pdm._shared import PdmObject


class OpenAPICodec:
    """
    OpenAPICodec serialises and deserialises PDM Entities to and from
    JSON.
    """

    @staticmethod
    def loads(klass: Type[PdmObject], json_data: str) -> PdmObject:
        return klass.model_validate_json(json_data)

    @staticmethod
    def dumps(entity: PdmObject) -> str:
        return entity.model_dump_json()

    @staticmethod
    def load_from_file(klass: Type[PdmObject], path: PathLike[str]):
        """
        Load an instance of a generated class from disk.

        :param klass: the class to create from the file
        :param path: the path to the file
        :return: an instance of class
        """
        with open(path, "r", encoding="utf-8") as json_file:
            json_data = json_file.read()
            return OpenAPICodec.loads(klass, json_data)


CODEC = OpenAPICodec()
