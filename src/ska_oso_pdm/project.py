from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import Metadata, PdmObject, ProjectID


class Author(PdmObject):
    """
    The author of a Proposal
    """

    pis: list[str] = Field(default_factory=list)
    cois: list[str] = Field(default_factory=list)


class ObservingBlock(PdmObject):
    obs_block_id: str
    name: Optional[str] = None
    sbd_ids: list[str] = Field(default_factory=list)


class Project(PdmObject):
    interface: Optional[str] = None
    prj_id: Optional[ProjectID] = None
    name: Optional[str] = None
    metadata: Optional[Metadata] = None
    author: Optional[Author] = None
    obs_blocks: list[ObservingBlock] = Field(default_factory=list)
