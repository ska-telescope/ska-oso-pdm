from pydantic import Field

from ska_oso_pdm._shared import AstropyQuantity, PdmObject
from ska_oso_pdm._shared.ids import DataProductSdpID, ObservationSetID


class DataProductSDP(PdmObject):
    data_products_sdp_id: DataProductSdpID = Field(
        description="Unique identifier for the Data Products SDP class",
    )
    options: list[str] = Field(default_factory=list, description="Observatory options")
    observation_set_refs: list[ObservationSetID] = Field(description="Observation")
    image_size: AstropyQuantity = Field(description="Image Size")
    pixel_size: AstropyQuantity = Field(description="Pixel Size")
    weighting: str = Field(description="Weighting")
