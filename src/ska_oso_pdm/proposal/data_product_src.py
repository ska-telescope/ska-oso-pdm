from pydantic import Field

from ska_oso_pdm._shared import PdmObject
from ska_oso_pdm._shared.ids import DataProductSrcID


class DataProductSRC(PdmObject):
    data_products_src_id: DataProductSrcID = Field(
        description="Unique identifier for the Data Products SRC-Net class",
    )
