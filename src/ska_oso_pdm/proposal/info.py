from typing import List, Optional

from pydantic import Field

from ska_oso_pdm._shared import PdmObject, Target
from ska_oso_pdm.proposal.investigator import Investigator
from ska_oso_pdm.proposal.result import Result

from .data_product_sdp import DataProductSDP
from .data_product_src import DataProductSRC
from .document import Document
from .observation_set import ObservationSets


class ProposalType(PdmObject):
    """Represents the categories of a proposal."""

    main_type: str = Field(..., description="Type of the proposal")
    attributes: Optional[List[str]] = Field(
        default_factory=list, description="Optional sub-type of the proposal"
    )


class Info(PdmObject):
    title: str = Field(description="Title of the proposal")
    proposal_type: ProposalType = Field(
        ...,
        description="Proposal type and optional sub-type combination",
    )
    abstract: str = Field(default="", description="Abstract of the proposal")
    science_category: Optional[str] = Field(
        default=None, description="Science category of the proposal"
    )
    targets: List[Target] = Field(
        default_factory=list, description="List of targets associated with the proposal"
    )
    documents: List[Document] = Field(
        default_factory=list,
        description="List of documents associated with the proposal",
    )
    investigators: List[Investigator] = Field(
        default_factory=list,
        description="Information on the investigators that have been invited onto the proposal",
    )
    observation_sets: List[ObservationSets] = Field(
        default_factory=list,
        description="List of observations associated with the proposal",
    )
    data_product_sdps: List[DataProductSDP] = Field(
        default_factory=list,
        description="List of data products associated with the SDP",
    )
    data_product_src_nets: List[DataProductSRC] = Field(
        default_factory=list,
        description="List of data products associated with the SRC-Net",
    )
    result_details: Optional[List[Result]] = Field(
        default_factory=list,
        description="List of target/observation set combinations with SensCalc results",
    )
