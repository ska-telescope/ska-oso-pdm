from typing import Literal, Optional, Union

from pydantic import Field
from typing_extensions import Annotated

from ska_oso_pdm._shared import (
    AstropyQuantity,
    ObservationSetID,
    PdmObject,
    TelescopeType,
)
from ska_oso_pdm.proposal.result import ResultsType


class Supplied(PdmObject):
    supplied_type: ResultsType = Field(
        description="Indicates which option the quantity relates to"
    )
    quantity: AstropyQuantity


class ObservationTypeDetails(PdmObject):
    observation_type: str = Field(description="Observation type")
    bandwidth: AstropyQuantity = Field(description="Bandwidth")
    central_frequency: AstropyQuantity = Field(description="Central Frequency")
    supplied: Optional[Supplied] = Field(
        default=None, description="Integration Time or sensitivity object"
    )
    spectral_resolution: Optional[str] = Field(
        default=None, description="Spectral Resolution"
    )
    effective_resolution: Optional[str] = Field(
        default=None, description="Effective resolution"
    )
    image_weighting: Optional[str] = Field(None, description="Image Weighting")
    robust: Optional[str] = Field(
        None,
        description="a Briggs image weighting parameter to balance sensitivity and resolution",
    )
    spectral_averaging: Optional[str] = Field(None, description="Spectral Averaging")


class LowArray(PdmObject):
    array: Literal[TelescopeType.SKA_LOW] = TelescopeType.SKA_LOW
    subarray: str = Field(description="Low Sub Array")
    number_of_stations: int = Field(
        0, description="Number of stations to use in the observation", ge=0, le=512
    )


class MidArray(PdmObject):
    array: Literal[TelescopeType.SKA_MID] = TelescopeType.SKA_MID
    subarray: str = Field(description="Mid Sub Array")
    weather: int = Field(
        default=3, description="Amount of accepted rainfall in mm", ge=3, le=25
    )
    number_15_antennas: int = Field(
        default=0,
        description="Number of 15mm antennas to use in the observation",
        ge=0,
        le=133,
    )
    number_13_antennas: int = Field(
        default=0,
        description="Number of 13.5mm antennas to use in the observation",
        ge=0,
        le=64,
    )
    number_sub_bands: int = Field(
        default=0,
        description="Number of Sub bands",
        ge=0,
        le=32,
    )
    tapering: Optional[str] = Field(default=None, description="tapering")


ArrayUnion = Annotated[
    Union[
        MidArray,
        LowArray,
    ],
    Field(discriminator="array"),
]


class ObservationSets(PdmObject):
    observation_set_id: ObservationSetID = Field(
        description="Unique identifier for Observation Sets"
    )
    group_id: Optional[str] = Field(default=None, description="Grouping identifier")
    observing_band: str = Field(description="Observing Band")
    elevation: int = Field(
        default=15, description="Elevation from the horizon to be used", ge=15, le=90
    )
    array_details: ArrayUnion = Field(
        # default_factory=MidArray, - Info needs to have all fields with defaults for this
        description="Array details specific to the telescope selected",
    )
    observation_type_details: ObservationTypeDetails = Field(
        description="Observation type details"
    )
