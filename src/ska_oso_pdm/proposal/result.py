from typing import Annotated, Literal, Optional, Union

from pydantic import Field

from ska_oso_pdm._shared import AstropyQuantity, PdmObject, TerseStrEnum
from ska_oso_pdm._shared.ids import ObservationSetID, TargetID


class ResultsType(TerseStrEnum):
    INTEGRATION = "integration_time"
    SENSITIVITY = "sensitivity"


class SurfaceBrightnessSensitivity(PdmObject):
    continuum: Optional[float]
    spectral: Optional[float]
    unit: Optional[str]


class SynthesizedBeamSize(PdmObject):
    continuum: Optional[str]
    spectral: Optional[str]
    unit: Optional[str]


class Sensitivity(PdmObject):
    supplied_type: Literal[ResultsType.SENSITIVITY] = ResultsType.SENSITIVITY
    continuum: Optional[AstropyQuantity] = Field(description="Continuum observation")
    spectral: Optional[AstropyQuantity] = Field(description="Spectral observation")


class Integration(PdmObject):
    supplied_type: Literal[ResultsType.INTEGRATION] = ResultsType.INTEGRATION

    weighted_continuum_sensitivity: Optional[AstropyQuantity] = Field(
        description="weighted continuum sensitivity"
    )
    weighted_spectral_sensitivity: Optional[AstropyQuantity] = Field(
        description="weighted spectral sensitivity"
    )
    total_continuum_sensitivity: Optional[AstropyQuantity] = Field(
        description="Total continuum sensitivity"
    )
    total_spectral_sensitivity: Optional[AstropyQuantity] = Field(
        description="Total spectral sensitivity"
    )
    surface_brightness_sensitivity: SurfaceBrightnessSensitivity = Field(
        description="Surface brightness sensitivity"
    )


ResultsUnion = Annotated[
    Union[
        Integration,
        Sensitivity,
    ],
    Field(discriminator="supplied_type"),
]


class Result(PdmObject):
    observation_set_ref: ObservationSetID = Field(
        description="Observation set related to the results"
    )
    target_ref: TargetID = Field(description="Target related to the results")

    result: ResultsUnion = Field(
        description="results specific to the observation/target combination",
    )
    continuum_confusion_noise: AstropyQuantity = Field(
        description="continuum confusion noise"
    )
    synthesized_beam_size: SynthesizedBeamSize = Field(
        description="synthesized beam size"
    )
    spectral_confusion_noise: AstropyQuantity = Field(
        description="spectral confusion noise"
    )
