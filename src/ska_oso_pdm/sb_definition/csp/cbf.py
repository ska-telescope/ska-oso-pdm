"""
The ska_oso_pdm.sb_definition.csp.cbf module defines a Python
representation of CSP and FSP configurations.
"""

import enum
from typing import List, Optional

from pydantic import Field, model_validator
from typing_extensions import Self

from ska_oso_pdm._shared import PdmObject

__all__ = [
    "FSPConfiguration",
    "FSPFunctionMode",
    "CBFConfiguration",
]


# Global constants for FSP configurations
MAX_FSP_ID = 27
MAX_FREQUENCY_SLICE_ID = 26
MIN_CORR_BANDWIDTH = 0
MAX_CORR_BANDWIDTH = 6
MAX_TUPLE_COUNT_IN_CHANNEL_MAP = 20


class FSPFunctionMode(enum.Enum):
    """
    FSPFunctionMode is an enumeration of the available FSP modes.
    """

    CORR = "CORR"
    PSS_BF = "PSS-BF"
    PST_BF = "PST-BF"
    VLBI = "VLBI"


class FSPConfiguration(PdmObject):
    """
    FSPConfiguration defines the configuration for a CSP Frequency Slice
    Processor.

    Channel averaging map is an optional list of 20 x (int,int) tuples.

    :param fsp_id: FSP configuration ID [1..27]
    :param function_mode: FSP function mode
    :param frequency_slice_id: frequency slicer ID [1..26]
    :param zoom_factor: zoom factor [0..6]
    :param integration_factor: integer multiple of correlation integration time (140ms) [1..10]
    :param channel_averaging_map: Optional channel averaging map
    :param output_link_map: Optional output_link_map
    :param channel_offset: Optional channel offset value in integer
    :param zoom_window_tuning: Optional zoom window tuning value in integer
    """

    fsp_id: int = Field(ge=1, le=MAX_FSP_ID)
    function_mode: FSPFunctionMode
    frequency_slice_id: int = Field(ge=1, le=MAX_FREQUENCY_SLICE_ID)
    integration_factor: int = Field(ge=1, le=10)
    zoom_factor: int = Field(ge=MIN_CORR_BANDWIDTH, le=MAX_CORR_BANDWIDTH)
    channel_averaging_map: list[tuple[int, int]] = Field(
        default_factory=list, max_length=MAX_TUPLE_COUNT_IN_CHANNEL_MAP
    )
    output_link_map: list[tuple] = Field(default_factory=list)
    channel_offset: Optional[int] = None
    zoom_window_tuning: Optional[int] = None

    @model_validator(mode="after")
    def validate_zoom_window_tuning(self) -> Self:
        if self.zoom_window_tuning is None and self.zoom_factor > 0:
            raise ValueError(
                "Zoom window tuning cannot be None when Correlator bandwidth"
                " is greater than 0"
            )
        return self


class VLBIConfiguration(PdmObject):
    """
    Class to hold VLBI configurations.
    """


class CBFConfiguration(PdmObject):
    """
    Class to hold all FSP and VLBI configurations.

    :param fsps: List of the FSP configurations to set
    :param vlbi: the VLBI configuration to set (optional)
    """

    fsps: List[FSPConfiguration] = Field(default_factory=list)
    vlbi: Optional[VLBIConfiguration] = None
