"""
The ska_oso_pdm.sb_definition.csp.common module defines a Python representation of the
 CSP Common configuration specification
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import PdmObject

__all__ = [
    "CommonConfiguration",
]


class CommonConfiguration(PdmObject):
    """
    Class to hold the CSP sub-elements.

    :param subarray_id: an ID of sub-array device (optional)
    :param band_5_tuning: list of integer (optional)
    """

    subarray_id: Optional[int] = None
    band_5_tuning: list[float] = Field(default_factory=list)
