"""
The ska_oso_pdm.sb_definition.csp.midcbf module defines a Python
representation of CSP and FSP configurations.
"""

import enum
from typing import List, Optional

from astropy.units import Quantity
from pydantic import Field

from ska_oso_pdm._shared import AstropyQuantity, PdmObject


class ReceiverBand(enum.Enum):
    """
    ReceiverBand is an enumeration of SKA MID receiver bands.
    """

    BAND_1 = "1"
    BAND_2 = "2"
    BAND_5A = "5a"
    BAND_5B = "5b"


class ChannelAveragingFactor(enum.IntEnum):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    SIX = 6
    EIGHT = 8
    TWELVE = 12
    SIXTEEN = 16
    TWENTYFOUR = 24
    THIRTYONE = 31
    THIRTYTWO = 32
    FORTYEIGHT = 48


class CorrelationSPWConfiguration(PdmObject):
    """
    CorrelationSPWConfiguration defines the configuration for a CSP Frequency Slice
    Processor.

    :param swp_id: SWP configuration ID [1..27]
    :param logical_fsp_ids: Logical FPS Ids
    :param receptors: list of receptors
    :param zoom_factor: zoom factor [0..6]
    :param centre_frequency: Centre Frequency
    :param number_of_channels: Number of Channels
    :param channel_averaging_factor: Channel Average Factor
    :param time_integration_factor: integer multiple of correlation integration time (140ms) [1..10]
    """

    spw_id: int = Field(ge=1, le=27)
    logical_fsp_ids: list[int]
    receptors: list[str] = Field(default_factory=list)
    zoom_factor: int = Field(ge=0, le=6)
    centre_frequency: float
    number_of_channels: int
    channel_averaging_factor: ChannelAveragingFactor = ChannelAveragingFactor.ONE
    time_integration_factor: int = Field(ge=1, le=10)


class Subband(PdmObject):
    """
    Class to hold sub-band

    :param band_5_tuning: Band 5 Tuning
    :param frequency_slice_offset: Optional Frequency Slice Offset in AstropyQuantity
    :param correlation_spws: List of Correlation Spws
    """

    band_5_tuning: Optional[float] = None
    frequency_slice_offset: AstropyQuantity = Quantity(0, "MHz")
    correlation_spws: List[CorrelationSPWConfiguration] = Field(default_factory=list)


class MidCBFConfiguration(PdmObject):
    """
    Class to hold all Correlation SPW configurations.

    :param frequency_band: Frequency Band
    :param subbands: list of subbands configurations
    """

    frequency_band: ReceiverBand
    subbands: List[Subband] = Field(default_factory=list)
