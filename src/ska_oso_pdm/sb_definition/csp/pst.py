"""
The ska_oso_pdm.sb_definition.csp.pst module defines a Python
representation of a PST Configuration.
"""

from ska_oso_pdm._shared import PdmObject

__all__ = ["PSTConfiguration"]


class PSTConfiguration(PdmObject):
    """
    Class to hold PST configurations.
    """
