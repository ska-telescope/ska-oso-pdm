"""
The ska_oso_pdm.sb_definition.csp.subarray module defines
a Python representation of the Subarray configuration
"""

from ska_oso_pdm._shared import PdmObject

__all__ = ["SubarrayConfiguration"]


class SubarrayConfiguration(PdmObject):
    """
    Class to hold the parameters relevant only for the current sub-array device.

    :param sub-array_name: Name of the sub-array
    """

    subarray_name: str
