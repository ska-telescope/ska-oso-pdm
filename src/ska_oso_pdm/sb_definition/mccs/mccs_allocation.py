"""
The entities module provides simple Python representations of the structured
request and response for the TMC CentralNode.AssignResources and Configure commands.
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import MCCSAllocationID, PdmObject, SubArrayLOW

__all__ = ["Aperture", "MCCSAllocation", "SubarrayBeamConfiguration"]

MAX_SUBARRAY_BEAM_ID = 48
MAX_STATION_ID = 511


class Aperture(PdmObject):
    """
    Aperture specifies how the aperture of a subarray beam should be
    configured.

    :param station_id: ID of station
    :param substation_id: ID of sub station
    :param weighting_key: weighting key value
    """

    station_id: int = Field(ge=0, le=MAX_STATION_ID)
    substation_id: int
    weighting_key: str = "uniform"


class SubarrayBeamConfiguration(PdmObject):
    """
    SubarrayBeamConfiguration specifies how SKA LOW sub-array should be
    configured.

    :param apertures: list of aperture in this subarray
    :param subarray_beam_id: ID of subarray beam this config maps to
    :param number_of_channels: number of channels in this subarray beam,
        this field represents the maximum number of channels that the SBD
        will require. It should be calculated from the set of CSP
        configurations instead of being defined by the user but is optional
        in SubarrayBeamConfiguration for now for backwards compatibility.
    """

    apertures: list[Aperture]
    subarray_beam_id: int = Field(ge=1, le=MAX_SUBARRAY_BEAM_ID)
    number_of_channels: Optional[int] = Field(
        default=None,
        deprecated=(
            "number_of_channels will be removed in the future as it "
            "should be calculated from the set of CSP configurations "
            "instead of being defined by the user"
        ),
    )


class MCCSAllocation(PdmObject):
    """
    MCCSAllocation is a Python representation of the MCCS allocation segment
    of a scheduling block.

    :param mccs_allocation_id: stations ID's to allocate
    :param selected_subarray_definition: name of pre-defined/standard configuration
    :param subarray_beams: subarray beam list
    """

    mccs_allocation_id: MCCSAllocationID
    selected_subarray_definition: SubArrayLOW
    subarray_beams: list[SubarrayBeamConfiguration]
