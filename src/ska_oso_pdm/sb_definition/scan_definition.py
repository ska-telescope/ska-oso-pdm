"""
The ska_oso_pdm.sb_definition.scan_definition module defines
simple Python representation of a single observation scan
"""

__all__ = ["ScanDefinition"]

from typing import Optional

from ska_oso_pdm._shared import (
    PdmObject,
    ScanDefinitionID,
    TargetID,
    TerseStrEnum,
    TimedeltaMs,
)

from .csp.csp_configuration import CSPConfigurationID
from .dish.dish_allocation import DishAllocationID
from .mccs.mccs_allocation import MCCSAllocationID
from .sdp import ScanTypeID


class PointingCorrection(TerseStrEnum):
    """
    Operation to apply to the pointing correction model.

    MAINTAIN: continue applying the current pointing correction model
    UPDATE: wait for (if necessary) and apply new pointing calibration solution
    RESET: reset the applied pointing correction to the pointing model defaults
    """

    MAINTAIN = "MAINTAIN"
    UPDATE = "UPDATE"
    RESET = "RESET"


class ScanDefinition(PdmObject):
    """
    ScanDefinition represents the instrument configuration for a single scan.

    :param scan_definition_id: the unique ID for this scan definition
    :param scan_duration_ms: scan duration
    :target_ref: ID of target to observe
    :mccs_allocation_ref: ID of MCCS Config
    :target_beam_configuration_refs: SKA LOW sub-array beam configurations to apply during this scan.
    :dish_allocation_ref: SKA MID dish configuration ID during this scan.
    :scan_type_ref: SKA MID scan type ID
    :csp_configuration_ref: SKA MID  Central Signal Processor ID
    :pointing_correction: operation to apply to the pointing correction model.
    """

    scan_definition_id: Optional[ScanDefinitionID] = None
    scan_duration_ms: TimedeltaMs
    target_ref: Optional[TargetID] = None
    mccs_allocation_ref: Optional[MCCSAllocationID] = None
    dish_allocation_ref: Optional[DishAllocationID] = None
    scan_type_ref: Optional[ScanTypeID] = None
    csp_configuration_ref: Optional[CSPConfigurationID] = None
    scan_intent: Optional[str] = None
    pointing_correction: PointingCorrection = PointingCorrection.MAINTAIN
