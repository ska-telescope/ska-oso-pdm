"""
The ska_oso_pdm.sb_definition.sdp.beam module defines a Python representation of
beams for SDP configuration.
"""

from enum import Enum
from typing import Optional

from ska_oso_pdm._shared import BeamID, PdmObject

__all__ = ["Beam", "BeamFunction"]


class BeamFunction(Enum):
    """
    Enumeration of possible functions for an SDP Beam.
    """

    VISIBILITIES = "visibilities"
    PULSAR_SEARCH = "pulsar search"
    PULSAR_TIMING = "pulsar timing"
    VLBI = "vlbi"
    TRANSIENT_BUFFER = "transient buffer"


class Beam(PdmObject):
    """
    Class that defines an SDP Beam configuration.
    """

    beam_id: BeamID
    function: BeamFunction
    search_beam_id: Optional[int] = None
    timing_beam_id: Optional[int] = None
    vlbi_beam_id: Optional[int] = None
