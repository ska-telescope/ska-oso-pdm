"""
The ska_oso_pdm.sb_definition.sdp.channels module defines a Python representation of
a channel configuration for SDP configuration.
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import ChannelsID, PdmObject, SpectralWindowID

__all__ = ["SpectralWindow", "Channels"]


class SpectralWindow(PdmObject):
    """
    Class to hold Spectral Windows configuration
    """

    spectral_window_id: SpectralWindowID
    count: int
    start: int
    freq_min: float
    freq_max: float
    stride: Optional[int] = None
    link_map: list[tuple] = Field(default_factory=list)


class Channels(PdmObject):
    """
    Class to hold Channel configuration
    """

    channels_id: ChannelsID
    spectral_windows: list[SpectralWindow]
