"""
The ska_oso_pdm.sb_definition.sdp.execution_block module defines a Python
object model for the Execution Block JSON string passed to
CentralNode.AssignResources.
"""

from pydantic import Field

from ska_oso_pdm._shared import ExecutionBlockID, PdmObject

from .beam import Beam
from .channels import Channels
from .polarisation import Polarisation
from .scan_type import ScanType

__all__ = ["ExecutionBlock"]


class ExecutionBlock(PdmObject):
    """
    ExecutionBlock captures the Execution Block resources and pipeline configuration
    required to process an execution block.

    :param eb_id: execution block ID
    :param max_length: maximum length
    :param context: context
    :param beams: list of Beams
    :param scan_types: list of EB scan types
    :param channels: list of EB channels
    :param polarisations: list of polarisations
    """

    eb_id: ExecutionBlockID
    max_length: float
    context: dict = Field(default_factory=dict)
    beams: list[Beam]
    scan_types: list[ScanType]
    channels: list[Channels]
    polarisations: list[Polarisation]
