"""
The ska_oso_pdm.sb_definition.sdp.polarisation module defines a Python representation of
polarisations for SDP configuration.
"""

from ska_oso_pdm._shared import PdmObject, PolarisationID

__all__ = ["Polarisation"]


class Polarisation(PdmObject):
    """
    Class that defines an SDP Polarisation configuration
    """

    polarisations_id: PolarisationID
    corr_type: list[str]
