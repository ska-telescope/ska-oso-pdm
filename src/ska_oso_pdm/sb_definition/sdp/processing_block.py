"""
The ska_oso_pdm.sb_definition.sdp.processing_block module defines a Python representation of
a processing block for SDP configuration.
"""

from enum import Enum

from pydantic import Field

from ska_oso_pdm._shared import PdmObject, ProcessingBlockID, SBInstanceID

__all__ = [
    "Script",
    "ProcessingBlock",
    "PbDependency",
    "ScriptKind",
    "ProcessingBlockID",
]


class ScriptKind(Enum):
    """
    Enumeration class to hold the kind of processing script
    """

    REALTIME = "realtime"
    BATCH = "batch"


class Script(PdmObject):
    """
    Class to hold Script for ProcessingBlock
    """

    name: str
    kind: ScriptKind
    version: str


class PbDependency(PdmObject):
    """
    Class to hold Dependencies for ProcessingBlock
    """

    pb_ref: ProcessingBlockID
    kind: list[str]


class ProcessingBlock(PdmObject):
    """
    Class to hold ProcessingBlock configuration
    """

    pb_id: ProcessingBlockID
    script: Script
    sbi_refs: list[SBInstanceID] = Field(default_factory=list)
    parameters: dict = Field(default_factory=dict)
    dependencies: list[PbDependency] = Field(default_factory=list)
