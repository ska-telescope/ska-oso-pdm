"""
The ska_oso_pdm.sb_definition.sdp.resources module defines a Python representation of
a Resources for SDP configuration.
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import PdmObject

__all__ = ["Resources"]


class Resources(PdmObject):
    """
    Class to hold SDP Resources
    """

    csp_links: list[int] = Field(default_factory=list)
    receptors: list[str] = Field(default_factory=list)
    receive_nodes: Optional[int] = None
