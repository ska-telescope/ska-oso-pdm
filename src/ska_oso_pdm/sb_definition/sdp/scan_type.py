"""
The ska_oso_pdm.sb_definition.sdp.scan_type module defines a Python representation of
a scan type for SDP configuration.
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import (
    BeamID,
    ChannelsID,
    PdmObject,
    PolarisationID,
    ScanTypeID,
    TargetID,
)

__all__ = ["ScanType", "BeamMapping"]


class BeamMapping(PdmObject):
    """
    Class to hold mapping of beam parameters to scans
    """

    beam_ref: BeamID
    field_ref: Optional[TargetID] = None
    channels_ref: Optional[ChannelsID] = None
    polarisations_ref: Optional[PolarisationID] = None


class ScanType(PdmObject):
    """
    Class to hold ScanType configuration
    """

    scan_type_id: ScanTypeID
    derive_from: Optional[str] = None
    beams: list[BeamMapping] = Field(default_factory=list)
