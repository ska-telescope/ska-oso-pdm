"""
The ska_oso_pdm.sb_definition.sdp.sdp_configuration module defines a Python
object model for the SDP configuration JSON string passed to
CentralNode.AssignResources.
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import PdmObject

from .execution_block import ExecutionBlock
from .processing_block import ProcessingBlock
from .resources import Resources

__all__ = ["SDPConfiguration"]


class SDPConfiguration(PdmObject):
    """
    SDPConfiguration captures the SDP resources and pipeline configuration
    required to process an execution block.

    :param execution_block: the SDP ExecutionBlock object
    :param resources: external resources
    :param processing_blocks: list of SDP ProcessingBlock objects
    """

    execution_block: Optional[ExecutionBlock] = None
    resources: Optional[Resources] = None
    processing_blocks: list[ProcessingBlock] = Field(default_factory=list)
