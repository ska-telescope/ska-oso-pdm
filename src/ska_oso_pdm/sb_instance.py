from datetime import datetime, timezone
from typing import Optional

from pydantic import AwareDatetime, Field

from ska_oso_pdm._shared import (
    ExecutionBlockID,
    FunctionArgs,
    Metadata,
    PdmObject,
    SBDefinitionID,
    SBInstanceID,
    TelescopeType,
)


class ActivityCall(PdmObject):

    activity_ref: Optional[str] = None
    executed_at: AwareDatetime = Field(
        default_factory=lambda: datetime.now(timezone.utc)
    )
    runtime_args: list[FunctionArgs] = Field(default_factory=list)


class SBInstance(PdmObject):

    interface: Optional[str] = None
    sbi_id: Optional[SBInstanceID] = None
    metadata: Optional[Metadata] = None
    telescope: TelescopeType
    sbd_ref: Optional[SBDefinitionID] = None
    sbd_version: Optional[int] = None
    eb_ref: Optional[ExecutionBlockID] = None
    subarray_id: Optional[int] = None
    activities: list[ActivityCall] = Field(default_factory=list)
