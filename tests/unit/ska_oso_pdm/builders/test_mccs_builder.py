from ska_oso_pdm.builders.mccs_builder import (
    ApertureBuilder,
    MCCSAllocationBuilder,
    SubarrayBeamConfigurationBuilder,
)
from ska_oso_pdm.sb_definition import SubArrayLOW


def test_mccs_builder_defaults():
    default_mccs_allocation = MCCSAllocationBuilder()
    assert default_mccs_allocation.selected_subarray_definition is SubArrayLOW.AA05_ALL
    assert len(default_mccs_allocation.subarray_beams) == 1
    assert len(default_mccs_allocation.subarray_beams[0].apertures) == 4
    assert len(default_mccs_allocation.subarray_beams[0].apertures) == 4


def test_mccs_builder_with_stations():
    custom_stations = [13, 25, 24]
    default_mccs_allocation = MCCSAllocationBuilder(stations=custom_stations)
    assert default_mccs_allocation.selected_subarray_definition is SubArrayLOW.CUSTOM
    assert len(default_mccs_allocation.subarray_beams) == 1
    assert len(default_mccs_allocation.subarray_beams[0].apertures) == len(
        custom_stations
    )
    assert [
        aperture.station_id
        for aperture in default_mccs_allocation.subarray_beams[0].apertures
    ] == custom_stations


def test_mccs_builder_does_not_overwrite_subarraybeams():
    subarray_beam_configs = [
        SubarrayBeamConfigurationBuilder(apertures=[ApertureBuilder(station_id=354)])
    ]
    mccs_allocation = MCCSAllocationBuilder(subarray_beams=subarray_beam_configs)
    assert len(mccs_allocation.subarray_beams) == 1
    assert len(mccs_allocation.subarray_beams[0].apertures) == 1
    assert mccs_allocation.subarray_beams[0].apertures[0].station_id == 354
