"""
Unit tests for the ska_oso_pdm.csp.cbf module.
"""

from contextlib import nullcontext
from typing import Any, ContextManager

import pytest
from pydantic import ValidationError

from tests.unit.ska_oso_pdm.serialisation_cases.csp.cbf import FSPConfigurationFactory

DOES_NOT_RAISE = nullcontext()


def _idfn(value: Any) -> str:
    if hasattr(value, "__enter__"):
        if isinstance(value, nullcontext):
            return "DOES_NOT_RAISE"
        else:
            return value.expected_exception.__name__
    return str(value)


@pytest.mark.parametrize(
    ("field_name", "value", "expectation"),
    (
        ("fsp_id", 1, DOES_NOT_RAISE),
        ("fsp_id", 27, DOES_NOT_RAISE),
        ("fsp_id", 0, pytest.raises(ValidationError)),
        ("fsp_id", 28, pytest.raises(ValidationError)),
        ("frequency_slice_id", 1, DOES_NOT_RAISE),
        ("frequency_slice_id", 26, DOES_NOT_RAISE),
        ("frequency_slice_id", 0, pytest.raises(ValidationError)),
        ("frequency_slice_id", 27, pytest.raises(ValidationError)),
        ("zoom_factor", 0, DOES_NOT_RAISE),
        ("zoom_factor", 6, DOES_NOT_RAISE),
        ("zoom_factor", -1, pytest.raises(ValidationError)),
        ("zoom_factor", 7, pytest.raises(ValidationError)),
        ("integration_factor", 1, DOES_NOT_RAISE),
        ("integration_factor", 10, DOES_NOT_RAISE),
        ("integration_factor", 0, pytest.raises(ValidationError)),
        ("integration_factor", 11, pytest.raises(ValidationError)),
        ("zoom_window_tuning", None, pytest.raises(ValidationError)),
        ("channel_averaging_map", [(1, 1)] * 20, DOES_NOT_RAISE),
        ("channel_averaging_map", [(1, 1)] * 21, pytest.raises(ValidationError)),
    ),
    ids=_idfn,
)
def test_field_validation(field_name: str, value: Any, expectation: ContextManager):
    with expectation:
        FSPConfigurationFactory(**{field_name: value})
