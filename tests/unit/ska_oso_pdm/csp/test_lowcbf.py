"""
Unit tests for the ska_oso_pdm.csp.lowcbf module.
"""

from contextlib import nullcontext
from datetime import timedelta
from typing import Any, ContextManager

import pytest
from pydantic import ValidationError

from tests.unit.ska_oso_pdm.serialisation_cases.csp.lowcbf import CorrelationFactory

DOES_NOT_RAISE = nullcontext()


def _idfn(value: Any) -> str:
    if hasattr(value, "__enter__"):
        if isinstance(value, nullcontext):
            return "DOES_NOT_RAISE"
        else:
            return value.expected_exception.__name__
    return str(value)


@pytest.mark.parametrize(
    ("field_name", "value", "expectation"),
    (
        ("zoom_factor", 1, DOES_NOT_RAISE),
        ("zoom_factor", 7, DOES_NOT_RAISE),
        ("zoom_factor", 8, pytest.raises(ValidationError)),
        ("zoom_factor", -1, pytest.raises(ValidationError)),
        ("integration_time_ms", 283, DOES_NOT_RAISE),
        ("integration_time_ms", 849, DOES_NOT_RAISE),
        ("integration_time_ms", timedelta(milliseconds=283), DOES_NOT_RAISE),
        ("integration_time_ms", timedelta(microseconds=849000), DOES_NOT_RAISE),
        ("integration_time_ms", 100, pytest.raises(ValueError)),
        ("integration_time_ms", timedelta(hours=1), pytest.raises(ValueError)),
    ),
    ids=_idfn,
)
def test_field_validation(field_name: str, value: Any, expectation: ContextManager):
    with expectation:
        CorrelationFactory(**{field_name: value})
