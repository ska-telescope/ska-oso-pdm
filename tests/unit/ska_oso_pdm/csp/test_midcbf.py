"""
Unit tests for the ska_oso_pdm.csp.midcbf module.
"""

from contextlib import nullcontext
from typing import Any, ContextManager

import pytest
from pydantic import ValidationError

from tests.unit.ska_oso_pdm.serialisation_cases.csp.midcbf import (
    CorrelationSPWConfigurationFactory,
)

DOES_NOT_RAISE = nullcontext()


def _idfn(value: Any) -> str:
    if hasattr(value, "__enter__"):
        if isinstance(value, nullcontext):
            return "DOES_NOT_RAISE"
        else:
            return value.expected_exception.__name__
    return str(value)


@pytest.mark.parametrize(
    ("field_name", "value", "expectation"),
    (
        ("spw_id", 1, DOES_NOT_RAISE),
        ("spw_id", 27, DOES_NOT_RAISE),
        ("spw_id", 0, pytest.raises(ValidationError)),
        ("spw_id", 28, pytest.raises(ValidationError)),
        ("zoom_factor", 0, DOES_NOT_RAISE),
        ("zoom_factor", 6, DOES_NOT_RAISE),
        ("zoom_factor", -1, pytest.raises(ValidationError)),
        ("zoom_factor", 7, pytest.raises(ValidationError)),
        ("time_integration_factor", 1, DOES_NOT_RAISE),
        ("time_integration_factor", 10, DOES_NOT_RAISE),
        ("time_integration_factor", 0, pytest.raises(ValidationError)),
        ("time_integration_factor", 11, pytest.raises(ValidationError)),
    ),
    ids=_idfn,
)
def test_field_validation(field_name: str, value: Any, expectation: ContextManager):
    with expectation:
        CorrelationSPWConfigurationFactory(**{field_name: value})
