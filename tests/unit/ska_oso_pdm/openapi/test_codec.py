"""
Unit tests for the Codec class.
"""

from unittest.mock import MagicMock, mock_open, patch

import pytest

from ska_oso_pdm import PdmObject
from ska_oso_pdm.openapi import CODEC

FAKE_JSON = '{"fake": "json"}'


@pytest.fixture(name="mock_pdm_class")
def fixture_mock_pdm_class():
    return MagicMock(spec=PdmObject)


def test_loads(mock_pdm_class: MagicMock):
    """
    Verify that the codec invokes the expected deserialisation methods.
    """
    result = CODEC.loads(mock_pdm_class, FAKE_JSON)
    mock_pdm_class.model_validate_json.assert_called_once_with(FAKE_JSON)
    assert result is mock_pdm_class.model_validate_json.return_value


def test_dumps(mock_pdm_class: MagicMock):
    """
    Verify that the codec invokes the expected serialisation methods.
    """
    mock_instance = mock_pdm_class()
    result = CODEC.dumps(mock_instance)
    mock_instance.model_dump_json.assert_called_once_with()
    assert result is mock_instance.model_dump_json.return_value


def test_load_from_file(mock_pdm_class: MagicMock):
    """
    Test that loading a file from disk reads from a file and calls
    model_validate_json...
    """
    with patch("builtins.open", mock_open(read_data=FAKE_JSON)) as mock_file:
        result = CODEC.load_from_file(mock_pdm_class, "somefile.json")
        mock_file.assert_called_with("somefile.json", "r", encoding="utf-8")
    mock_pdm_class.model_validate_json.assert_called_once_with(FAKE_JSON)
    assert result is mock_pdm_class.model_validate_json.return_value
