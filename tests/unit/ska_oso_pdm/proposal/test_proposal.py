from contextlib import nullcontext as does_not_raise
from datetime import datetime, timezone

import pytest
from pydantic import ValidationError

from ska_oso_pdm import Proposal
from tests.unit.ska_oso_pdm.serialisation_cases.proposal.proposal import PROPOSAL_DRAFT

DATETIME = datetime(2024, 10, 17, tzinfo=timezone.utc)
VALUES = PROPOSAL_DRAFT.model_dump()


@pytest.mark.parametrize(
    ("submitted_on", "submitted_by", "expectation"),
    (
        (None, None, does_not_raise()),
        (None, "", does_not_raise()),
        (DATETIME, "Brendan", does_not_raise()),
        (None, "Brendan", pytest.raises(ValidationError)),
        (DATETIME, None, pytest.raises(ValidationError)),
    ),
)
def test_submitted_xor(submitted_on, submitted_by, expectation):
    """Verifies model validator enforces submitted_on/by must be set or not as a pair."""
    values = dict(VALUES, submitted_on=submitted_on, submitted_by=submitted_by)
    with expectation:
        Proposal(**values)
