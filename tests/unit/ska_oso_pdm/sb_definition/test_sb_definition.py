"""
Unit tests for the ska_oso_pdm.sb_definition module.
"""

from datetime import datetime, timedelta, timezone

import pytest

from ska_oso_pdm._shared import Metadata
from ska_oso_pdm.sb_definition.sb_definition import (
    SBD_SCHEMA_URI,
    SBDefinition,
    TelescopeType,
)


def test_sb_definitions_no_sbd_id():
    """
    Verify an sbd_id of None is valid
    """
    # Create shared metadata to avoid different datetime.now() comparisons
    metadata = Metadata()
    sbd_1 = SBDefinition(sbd_id=None, metadata=metadata)

    assert sbd_1.sbd_id is None


def test_sb_definition_interface_is_defined():
    """
    Verify that the SBDefinition interface value is set
    by default.
    """
    sbd = SBDefinition()
    assert sbd.interface == SBD_SCHEMA_URI


def test_telescope_type_is_set_correctly():
    """
    Verify that telescope type is set correctly
    """
    assert TelescopeType.SKA_LOW.value == "ska_low"
    assert TelescopeType.SKA_MID.value == "ska_mid"
    assert TelescopeType.MEERKAT.value == "MeerKAT"


def test_metadata():
    """
    Test a Metadata Object is created correctly and has a correct timestamp
    """
    delta = timedelta(seconds=1)
    metadata = Metadata()

    # pylint: disable=no-member
    assert metadata.created_on.timestamp() == pytest.approx(
        datetime.now(timezone.utc).timestamp(), abs=delta.total_seconds()
    )

    assert metadata.version == 1
