from itertools import chain

from .cbf import TEST_CASES as cbf_cases
from .common import TEST_CASES as common_cases
from .csp_configuration import TEST_CASES as csp_config_cases
from .lowcbf import TEST_CASES as lowcbf_cases
from .midcbf import TEST_CASES as midcbf_cases
from .subarray import TEST_CASES as subarray_cases

TEST_CASES = tuple(
    chain(
        cbf_cases,
        common_cases,
        csp_config_cases,
        lowcbf_cases,
        midcbf_cases,
        subarray_cases,
    )
)
