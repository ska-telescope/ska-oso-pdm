"""
Tests for serialisations behavior in the cbf module to verify that classes
related to MID CBF configurations are dumped and loaded correctly.
"""

import functools
import json

import pytest

from ska_oso_pdm.sb_definition.csp.cbf import (
    CBFConfiguration,
    FSPConfiguration,
    FSPFunctionMode,
)
from tests.unit.ska_oso_pdm.utils import lbl

FSPConfigurationFactory = functools.partial(
    FSPConfiguration,
    fsp_id=2,
    function_mode=FSPFunctionMode.CORR,
    frequency_slice_id=2,
    integration_factor=1,
    zoom_factor=1,
    channel_averaging_map=[(0, 2), (744, 0)],
    channel_offset=744,
    output_link_map=[(0, 4), (200, 5)],
    zoom_window_tuning=4700000,
)

CBFConfigurationFactory = functools.partial(
    CBFConfiguration, fsps=[FSPConfigurationFactory()]
)

FSP_CONFIG_JSON = {
    "fsp_id": 2,
    "function_mode": "CORR",
    "frequency_slice_id": 2,
    "integration_factor": 1,
    "zoom_factor": 1,
    "output_link_map": [[0, 4], [200, 5]],
    "channel_averaging_map": [[0, 2], [744, 0]],
    "channel_offset": 744,
    "zoom_window_tuning": 4700000,
}

CBF_CONFIG_JSON = {"fsps": [FSP_CONFIG_JSON]}

TEST_CASES = (
    pytest.param(
        json.dumps(FSP_CONFIG_JSON),
        FSPConfigurationFactory,
        id=lbl("fsp_configuration"),
    ),
    pytest.param(
        json.dumps(CBF_CONFIG_JSON),
        CBFConfigurationFactory,
        id=lbl("cbf_configuration"),
    ),
)
