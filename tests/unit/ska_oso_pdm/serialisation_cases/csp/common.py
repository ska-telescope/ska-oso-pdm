"""
tests for the csp configuration schema to validate the
conversion between the JSON and Python representations
of the csp configuration section of an SKA Scheduling Block
"""

import json
from functools import partial

import pytest

from ska_oso_pdm.sb_definition.csp import CommonConfiguration
from tests.unit.ska_oso_pdm.utils import lbl

CommonConfigurationFactory = partial(
    CommonConfiguration, subarray_id=1, band_5_tuning=[5.85, 7.25]
)

CSP_COMMON_CONFIG_REFERENCE_JSON = {"subarray_id": 1, "band_5_tuning": [5.85, 7.25]}

TEST_CASES = (
    pytest.param(
        json.dumps(CSP_COMMON_CONFIG_REFERENCE_JSON),
        CommonConfigurationFactory,
        id=lbl("csp_common_config"),
    ),
)
