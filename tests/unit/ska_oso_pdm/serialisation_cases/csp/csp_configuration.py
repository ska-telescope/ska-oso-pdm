"""
tests for the csp configuration schema to validate the
conversion between the JSON and Python representations
of the csp configuration section of an SKA Scheduling Block
"""

import json

import pytest

from ska_oso_pdm.sb_definition.csp import (
    CBFConfiguration,
    CSPConfiguration,
    FSPConfiguration,
    FSPFunctionMode,
    SubarrayConfiguration,
)
from tests.unit.ska_oso_pdm.utils import lbl

from ...utils import TEST_CSP_CONFIGURATION_ID
from .common import CSP_COMMON_CONFIG_REFERENCE_JSON, CommonConfigurationFactory
from .lowcbf import LOW_CBF_CONFIG_WITH_PST_JSON, LowCBFConfigurationFactory
from .midcbf import MidCBFConfigurationFactory

MID_CSP_CONFIG_JSON = {
    "config_id": TEST_CSP_CONFIGURATION_ID,
    "name": "csp config 123",
    "subarray": {"subarray_name": "science period 23"},
    "common": CSP_COMMON_CONFIG_REFERENCE_JSON,
    "cbf": {
        "fsps": [
            {
                "fsp_id": 1,
                "function_mode": "CORR",
                "frequency_slice_id": 1,
                "integration_factor": 1,
                "zoom_factor": 0,
                "channel_averaging_map": [[0, 2], [744, 0]],
                "channel_offset": 0,
                "output_link_map": [[0, 0], [200, 1]],
            },
            {
                "fsp_id": 2,
                "function_mode": "CORR",
                "frequency_slice_id": 2,
                "integration_factor": 1,
                "zoom_factor": 1,
                "zoom_window_tuning": 650000,
            },
        ]
    },
    "midcbf": {
        "frequency_band": "1",
        "subbands": [
            {
                "band_5_tuning": 12.3e9,
                "frequency_slice_offset": {"value": 0, "unit": "MHz"},
                "correlation_spws": [
                    {
                        "spw_id": 2,
                        "logical_fsp_ids": [0, 1],
                        "receptors": ["SKA063", "SKA001", "SKA100"],
                        "zoom_factor": 1,
                        "centre_frequency": 800e6,
                        "number_of_channels": 20000,
                        "channel_averaging_factor": 1,
                        "time_integration_factor": 1,
                    }
                ],
            }
        ],
    },
}

MID_CSP_CONFIG = CSPConfiguration(
    config_id=TEST_CSP_CONFIGURATION_ID,
    name="csp config 123",
    subarray=SubarrayConfiguration(subarray_name="science period 23"),
    common=CommonConfigurationFactory(),
    cbf=CBFConfiguration(
        fsps=[
            FSPConfiguration(
                fsp_id=1,
                function_mode=FSPFunctionMode.CORR,
                frequency_slice_id=1,
                integration_factor=1,
                zoom_factor=0,
                channel_averaging_map=[(0, 2), (744, 0)],
                channel_offset=0,
                output_link_map=[(0, 0), (200, 1)],
            ),
            FSPConfiguration(
                fsp_id=2,
                function_mode=FSPFunctionMode.CORR,
                frequency_slice_id=2,
                integration_factor=1,
                zoom_factor=1,
                zoom_window_tuning=650000,
            ),
        ]
    ),
    midcbf=MidCBFConfigurationFactory(),
)

LOW_CSP_CONFIG_JSON = {
    "config_id": TEST_CSP_CONFIGURATION_ID,
    "name": "csp config 123",
    "lowcbf": LOW_CBF_CONFIG_WITH_PST_JSON,
}

LOW_CSP_CONFIG = CSPConfiguration(
    config_id=TEST_CSP_CONFIGURATION_ID,
    name="csp config 123",
    lowcbf=LowCBFConfigurationFactory(),
)

TEST_CASES = (
    pytest.param(
        json.dumps(MID_CSP_CONFIG_JSON),
        MID_CSP_CONFIG.model_copy,
        id=lbl("mid_csp_configuration"),
    ),
    pytest.param(
        json.dumps(LOW_CSP_CONFIG_JSON),
        LOW_CSP_CONFIG.model_copy,
        id=lbl("low_csp_configuration"),
    ),
)
