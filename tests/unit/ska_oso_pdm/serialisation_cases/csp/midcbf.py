"""
Test cases to verify that classes related to MID CBF configurations
serialise to and load from JSON correctly.
"""

import functools
import json

import pytest

from ska_oso_pdm.sb_definition.csp.midcbf import (
    ChannelAveragingFactor,
    CorrelationSPWConfiguration,
    MidCBFConfiguration,
    ReceiverBand,
    Subband,
)
from tests.unit.ska_oso_pdm.utils import lbl

CorrelationSPWConfigurationFactory = functools.partial(
    CorrelationSPWConfiguration,
    spw_id=2,
    logical_fsp_ids=[0, 1],
    receptors=["SKA063", "SKA001", "SKA100"],
    zoom_factor=1,
    centre_frequency=800e6,
    number_of_channels=20e3,
    channel_averaging_factor=ChannelAveragingFactor.ONE,
    time_integration_factor=1,
)

SubbandFactory = functools.partial(
    Subband,
    band_5_tuning=12.3e9,
    correlation_spws=[CorrelationSPWConfigurationFactory()],
)

MidCBFConfigurationFactory = functools.partial(
    MidCBFConfiguration, frequency_band=ReceiverBand.BAND_1, subbands=[SubbandFactory()]
)

CORRELATION_SPW_CONFIG_JSON = {
    "spw_id": 2,
    "logical_fsp_ids": [0, 1],
    "receptors": ["SKA063", "SKA001", "SKA100"],
    "zoom_factor": 1,
    "centre_frequency": 800e6,
    "number_of_channels": 20e3,
    "channel_averaging_factor": 1.0,
    "time_integration_factor": 1,
}

SUBBAND_CONFIG_JSON = {
    "band_5_tuning": 12.3e9,
    "frequency_slice_offset": {"value": 0, "unit": "MHz"},
    "correlation_spws": [CORRELATION_SPW_CONFIG_JSON],
}

MID_CBF_CONFIG_JSON = {"frequency_band": "1", "subbands": [SUBBAND_CONFIG_JSON]}

TEST_CASES = (
    pytest.param(
        json.dumps(CORRELATION_SPW_CONFIG_JSON),
        CorrelationSPWConfigurationFactory,
        id=lbl("correlation_spws_configuration"),
    ),
    pytest.param(
        json.dumps(SUBBAND_CONFIG_JSON),
        SubbandFactory,
        id=lbl("subband_configuration"),
    ),
    pytest.param(
        json.dumps(MID_CBF_CONFIG_JSON),
        MidCBFConfigurationFactory,
        id=lbl("midcbf_configuration"),
    ),
)
