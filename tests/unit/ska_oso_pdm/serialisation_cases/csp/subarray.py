"""
Serialisation cases for the CSP subarray module.
"""

import json

import pytest

from ska_oso_pdm.sb_definition.csp.subarray import SubarrayConfiguration
from tests.unit.ska_oso_pdm.utils import lbl

TEST_CASES = (
    pytest.param(
        json.dumps({"subarray_name": "Test Subarray"}),
        lambda: SubarrayConfiguration(subarray_name="Test Subarray"),
        id=lbl("subarray_config"),
    ),
)
