"""
tests for the dish_configuration_schema to validate the
conversion between the JSON and Python representations
of an SKA Scheduling Block
"""

import pytest

from ska_oso_pdm.sb_definition.dish.dish_allocation import DishAllocation
from tests.unit.ska_oso_pdm.utils import lbl

VALID_DISH_ALLOCATION_JSON = '{"dish_allocation_id":"dish config 123", "dish_ids": ["SKA001", "SKA036", "SKA063", "SKA100"], "selected_subarray_definition": "AA0.5"}'

VALID_DISH_ALLOCATION = DishAllocation(
    dish_allocation_id="dish config 123",
    dish_ids=["SKA001", "SKA036", "SKA063", "SKA100"],
    selected_subarray_definition="AA0.5",
)


TEST_CASES = (
    pytest.param(
        VALID_DISH_ALLOCATION_JSON,
        VALID_DISH_ALLOCATION.model_copy,
        id=lbl("dish_allocation"),
    ),
)
