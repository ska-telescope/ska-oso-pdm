"""
Unit tests to validate the serialisation and deserialisation of
OSOExecutionBlocks.
"""

from datetime import datetime

import pytest

from ska_oso_pdm.execution_block import (
    ErrorWrapper,
    Metadata,
    OSOExecutionBlock,
    PythonArguments,
    RequestResponse,
    ResponseWrapper,
    TelescopeType,
)
from tests.unit.ska_oso_pdm.utils import lbl, load_string_from_file

SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")

OSO_EXECUTION_BLOCK_JSON = load_string_from_file("testfile_sample_execution_block.json")

metadata = Metadata(
    version=1,
    created_by="TestUser",
    created_on=SAMPLE_DATETIME,
    last_modified_by="TestUser",
    last_modified_on=SAMPLE_DATETIME,
    pdm_version="15.3.0",
)

OSO_EXECUTION_BLOCK = OSOExecutionBlock(
    metadata=metadata,
    interface="https://schema.skao.int/ska-oso-pdm-eb/0.1",
    eb_id="eb-mvp01-20220923-00001",
    telescope=TelescopeType.SKA_MID,
    sbd_ref="sbd-mvp01-20220923-00001",
    sbd_version=1,
    sbi_ref="sbi-mvp01-20220923-00001",
    request_responses=[
        RequestResponse(
            request="ska_oso_scripting.functions.devicecontrol.release_all_resources",
            request_args=PythonArguments(kwargs={"subarray_id": "1"}),
            status="OK",
            response=ResponseWrapper(result="this is a result"),
            request_sent_at=SAMPLE_DATETIME,
            response_received_at=SAMPLE_DATETIME,
        ),
        RequestResponse(
            request="ska_oso_scripting.functions.devicecontrol.scan",
            status="ERROR",
            error=ErrorWrapper(detail="this is an error"),
            request_sent_at=SAMPLE_DATETIME,
            response_received_at=SAMPLE_DATETIME,
        ),
    ],
)

# Timestamps in these test data are set to non-UTC to work around a temporary
# issue with timezone equality
#
# See https://github.com/pydantic/pydantic/issues/8683
#
VALID_EXECUTION_BLOCK_JSON = """
{
  "interface": "https://schema.skao.int/ska-oso-pdm-eb/0.1",
  "eb_id": "eb-mvp01-20220923-00001",
  "telescope": "ska_mid",
  "sbd_ref": "sbd-mvp01-20220923-00001",
  "sbd_version": 1,
  "sbi_ref": "sbi-mvp01-20220923-00001",
  "metadata": {
    "version": 1,
    "created_by": "DefaultUser",
    "created_on": "2022-03-28T15:43:53.971548+01:00",
    "last_modified_on": "2022-03-28T15:43:53.971548+01:00",
    "last_modified_by": "DefaultUser",
    "pdm_version": "15.3.0"
  },
  "request_responses": [
    {
      "request": "ska_oso_scripting.functions.devicecontrol.release_all_resources",
      "request_args": {
        "kwargs": {
          "subarray_id": "1"
        }
      },
      "status": "OK",
      "response": {
        "result": "this is a result"
      },
      "request_sent_at": "2022-09-23T15:43:53.971548+01:00",
      "response_received_at": "2022-09-23T15:43:53.971548+01:00"
    },
    {
      "request": "ska_oso_scripting.functions.devicecontrol.scan",
      "status": "ERROR",
      "error": {
        "detail": "this is an error"
      },
      "request_sent_at": "2022-09-23T15:43:53.971548+01:00"
    }
  ]
}
"""

VALID_EXECUTION_BLOCK = OSOExecutionBlock(
    interface="https://schema.skao.int/ska-oso-pdm-eb/0.1",
    eb_id="eb-mvp01-20220923-00001",
    telescope=TelescopeType.SKA_MID,
    sbd_ref="sbd-mvp01-20220923-00001",
    sbd_version=1,
    sbi_ref="sbi-mvp01-20220923-00001",
    metadata=Metadata(
        version=1,
        created_by="DefaultUser",
        created_on="2022-03-28T15:43:53.971548+01:00",
        last_modified_on="2022-03-28T15:43:53.971548+01:00",
        last_modified_by="DefaultUser",
        pdm_version="15.3.0",
    ),
    request_responses=[
        RequestResponse(
            request="ska_oso_scripting.functions.devicecontrol.release_all_resources",
            request_args=PythonArguments(kwargs={"subarray_id": "1"}),
            status="OK",
            response=ResponseWrapper(result="this is a result"),
            request_sent_at=datetime.fromisoformat("2022-09-23T15:43:53.971548+01:00"),
            response_received_at=datetime.fromisoformat(
                "2022-09-23T15:43:53.971548+01:00"
            ),
        ),
        RequestResponse(
            request="ska_oso_scripting.functions.devicecontrol.scan",
            status="ERROR",
            error=ErrorWrapper(detail="this is an error"),
            request_sent_at=datetime.fromisoformat("2022-09-23T15:43:53.971548+01:00"),
        ),
    ],
)


TEST_CASES = (
    pytest.param(
        VALID_EXECUTION_BLOCK_JSON,
        VALID_EXECUTION_BLOCK.model_copy,
        id=lbl("oso_execution_block1"),
    ),
    pytest.param(
        OSO_EXECUTION_BLOCK_JSON,
        OSO_EXECUTION_BLOCK.model_copy,
        # Copied from the former test_codec.py
        id=lbl("oso_execution_block2"),
    ),
)
