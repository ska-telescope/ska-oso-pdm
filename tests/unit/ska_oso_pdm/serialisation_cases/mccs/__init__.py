from itertools import chain

from .mccs_allocation import TEST_CASES as mccs_allocation_cases

TEST_CASES = tuple(chain(mccs_allocation_cases))
