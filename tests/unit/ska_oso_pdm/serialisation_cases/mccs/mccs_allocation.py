"""
tests for the mccs_allocation to validate the
conversion between the JSON and Python representations
of an SKA Scheduling Block
"""

import json

import pytest

from ska_oso_pdm.builders.mccs_builder import MCCSAllocationBuilder
from ska_oso_pdm.sb_definition import SubArrayLOW
from tests.unit.ska_oso_pdm.utils import lbl

MCCS_ALLOCATION = MCCSAllocationBuilder(
    mccs_allocation_id="mccs-allocation-74519",
    selected_subarray_definition=SubArrayLOW.AA05_ALL,
)

MCCS_ALLOCATION_JSON = {
    "mccs_allocation_id": "mccs-allocation-74519",
    "selected_subarray_definition": "AA0.5",
    "subarray_beams": [
        {
            "subarray_beam_id": 1,
            "apertures": [
                {"station_id": 345, "substation_id": 1, "weighting_key": "uniform"},
                {"station_id": 350, "substation_id": 1, "weighting_key": "uniform"},
                {"station_id": 352, "substation_id": 1, "weighting_key": "uniform"},
                {"station_id": 431, "substation_id": 1, "weighting_key": "uniform"},
            ],
        }
    ],
}

TEST_CASES = (
    pytest.param(
        json.dumps(MCCS_ALLOCATION_JSON),
        MCCS_ALLOCATION.model_copy,
        id=lbl("mccs_allocation"),
    ),
)
