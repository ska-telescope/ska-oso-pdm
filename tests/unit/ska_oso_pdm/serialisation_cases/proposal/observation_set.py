import json

import pytest
from astropy import units as u

from ska_oso_pdm.proposal import (
    LowArray,
    ObservationSets,
    ObservationTypeDetails,
    ResultsType,
    Supplied,
    TelescopeType,
)
from tests.unit.ska_oso_pdm.utils import lbl

OBSERVATION_SETS_JSON = {
    "observation_set_id": "low-001",
    "group_id": "2",
    "observing_band": "low_band",
    "elevation": 23,
    "array_details": {
        "array": "ska_low",
        "subarray": "AA0.5",
        "number_of_stations": 1,
    },
    "observation_type_details": {
        "observation_type": "continuum",
        "bandwidth": {"value": 0.0, "unit": "Hz"},
        "central_frequency": {"value": 0.0, "unit": "Hz"},
        "supplied": {
            "supplied_type": "integration_time",
            "quantity": {"value": 0.0, "unit": "s"},
        },
        "spectral_resolution": "DUMMY",
        "effective_resolution": "DUMMY",
        "image_weighting": "DUMMY",
        "spectral_averaging": "DUMMY",
    },
}

OBSERVATION_SETS = ObservationSets(
    observation_set_id="low-001",
    group_id="2",
    observing_band="low_band",
    elevation=23,
    array_details=LowArray(
        array=TelescopeType.SKA_LOW,
        subarray="AA0.5",
        number_of_stations=1,
        spectral_averaging="DUMMY",
    ),
    observation_type_details=ObservationTypeDetails(
        observation_type="continuum",
        bandwidth=u.Quantity(0, "Hz"),
        central_frequency=u.Quantity(0, "Hz"),
        supplied=Supplied(
            supplied_type=ResultsType.INTEGRATION,
            quantity=u.Quantity(0, "s"),
        ),
        spectral_resolution="DUMMY",
        effective_resolution="DUMMY",
        image_weighting="DUMMY",
        spectral_averaging="DUMMY",
    ),
)

TEST_CASES = [
    pytest.param(
        json.dumps(OBSERVATION_SETS_JSON),
        OBSERVATION_SETS.model_copy,
        id=lbl("observation_sets"),
    )
]
