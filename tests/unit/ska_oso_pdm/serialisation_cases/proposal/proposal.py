from datetime import datetime

import pytest
from astropy import units as u

from ska_oso_pdm import Target
from ska_oso_pdm.proposal import (
    EquatorialCoordinates,
    Info,
    Metadata,
    Proposal,
    ProposalType,
    RadialVelocity,
    RadialVelocityDefinition,
    RadialVelocityReferenceFrame,
    RadialVelocityUnits,
)
from tests.unit.ska_oso_pdm.utils import TEST_TARGET_ID, lbl, load_string_from_file

SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")


PROPOSAL_SUBMITTED_JSON = load_string_from_file(
    "testfile_sample_proposal_submitted.json"
)

METADATA = Metadata(
    version=1,
    created_by="TestUser",
    created_on=SAMPLE_DATETIME,
    last_modified_by="TestUser",
    last_modified_on=SAMPLE_DATETIME,
    pdm_version="15.3.0",
)

PROPOSAL_SUBMITTED = Proposal(
    prsl_id="prp-ska01-202204-01",
    status="submitted",
    submitted_on=SAMPLE_DATETIME,
    submitted_by="TestUser",
    investigator_refs=[
        "prp-ska01-202204-01",
    ],
    metadata=METADATA,
    cycle="SKA_5000_2023",
    info=Info(
        title="The Milky Way View",
        abstract="Pretty Looking frontend depends on hard work put into good wire-framing and requirement gathering",
        proposal_type=ProposalType(
            main_type="standard_proposal", attributes=["coordinated_proposal"]
        ),
        science_category="Science Category",
        targets=[
            Target(
                target_id=TEST_TARGET_ID,
                name="M28",
                reference_coordinate=EquatorialCoordinates(
                    ra=250.000, dec=30.000, unit=("deg", "deg")
                ),
                radial_velocity=RadialVelocity(
                    quantity=u.Quantity(
                        value=-12.345, unit=RadialVelocityUnits.M_PER_SEC
                    ),
                    definition=RadialVelocityDefinition.OPTICAL,
                    reference_frame=RadialVelocityReferenceFrame.LSRK,
                    redshift=1.2,
                ),
            ),
            Target(
                target_id=TEST_TARGET_ID,
                name="M1",
                reference_coordinate=EquatorialCoordinates(
                    ra=250.000, dec=-30.000, unit=("deg", "deg")
                ),
            ),
        ],
        documents=[
            {
                "document_id": "doc_ref_01",
                "uploaded_pdf": True,
            },
            {
                "document_id": "doc_ref_02",
                "uploaded_pdf": False,
            },
        ],
        investigators=[
            {
                "investigator_id": "prp-ska01-202204-01",
                "given_name": "Tony",
                "family_name": "Bennet",
                "email": "somewhere.vague@example.com",
                "organization": "",
                "for_phd": False,
                "status": "pending",
                "principal_investigator": True,
            }
        ],
        observation_sets=[
            {
                "observation_set_id": "mid-001",
                "group_id": "2",
                "elevation": 23,
                "observing_band": "mid_band_1",
                "array_details": {
                    "array": "ska_mid",
                    "subarray": "AA0.5",
                    "weather": 3,
                    "number_15_antennas": 0,
                    "number_13_antennas": 0,
                    "number_sub_bands": 0,
                    "tapering": "DUMMY",
                },
                "observation_type_details": {
                    "observation_type": "continuum",
                    "bandwidth": {"value": 0.0, "unit": "Hz"},
                    "central_frequency": {"value": 0.0, "unit": "Hz"},
                    "supplied": {
                        "supplied_type": "integration_time",
                        "quantity": {"value": 0.0, "unit": "nJy"},
                    },
                    "spectral_resolution": "DUMMY",
                    "effective_resolution": "DUMMY",
                    "image_weighting": "DUMMY",
                    "robust": "DUMMY",
                    "spectral_averaging": "DUMMY",
                },
                "details": "MID + Continuum",
            },
            {
                "observation_set_id": "mid-002",
                "group_id": "2",
                "elevation": 23,
                "observing_band": "mid_band_1",
                "array_details": {
                    "array": "ska_mid",
                    "subarray": "AA0.5",
                    "weather": 3,
                    "number_15_antennas": 0,
                    "number_13_antennas": 0,
                    "number_sub_bands": 0,
                    "tapering": "DUMMY",
                },
                "observation_type_details": {
                    "observation_type": "zoom",
                    "bandwidth": {"value": 0.0, "unit": "Hz"},
                    "central_frequency": {"value": 0.0, "unit": "Hz"},
                    "supplied": {
                        "supplied_type": "sensitivity",
                        "quantity": {"value": 0.0, "unit": "nJy"},
                    },
                    "spectral_resolution": "DUMMY",
                    "effective_resolution": "DUMMY",
                    "image_weighting": "DUMMY",
                    "robust": "DUMMY",
                    "spectral_averaging": "DUMMY",
                },
                "details": "MID + Zoom",
            },
            {
                "observation_set_id": "low-001",
                "group_id": "2",
                "elevation": 23,
                "observing_band": "low_band",
                "array_details": {
                    "array": "ska_low",
                    "subarray": "AA0.5",
                    "number_of_stations": 1,
                },
                "observation_type_details": {
                    "observation_type": "continuum",
                    "bandwidth": {"value": 0.0, "unit": "Hz"},
                    "central_frequency": {"value": 0.0, "unit": "Hz"},
                    "supplied": {
                        "supplied_type": "integration_time",
                        "quantity": {"value": 0.0, "unit": "nJy"},
                    },
                    "spectral_resolution": "DUMMY",
                    "effective_resolution": "DUMMY",
                    "image_weighting": "DUMMY",
                    "robust": "DUMMY",
                    "spectral_averaging": "DUMMY",
                },
                "details": "LOW + Continuum",
            },
            {
                "observation_set_id": "low-002",
                "group_id": "2",
                "elevation": 23,
                "observing_band": "low_band",
                "array_details": {
                    "array": "ska_low",
                    "subarray": "AA0.5",
                    "number_of_stations": 1,
                },
                "observation_type_details": {
                    "observation_type": "zoom",
                    "bandwidth": {"value": 0.0, "unit": "Hz"},
                    "central_frequency": {"value": 0.0, "unit": "Hz"},
                    "supplied": {
                        "supplied_type": "integration_time",
                        "quantity": {"value": 0.0, "unit": "nJy"},
                    },
                    "spectral_resolution": "DUMMY",
                    "effective_resolution": "DUMMY",
                    "image_weighting": "DUMMY",
                    "robust": "DUMMY",
                    "spectral_averaging": "DUMMY",
                },
            },
        ],
        data_product_sdps=[
            {
                "data_products_sdp_id": "SDP-1",
                "options": ["1", "2", "5"],
                "observation_set_refs": ["mid-001", "low-001"],
                "image_size": {"value": 0.0, "unit": "deg"},
                "pixel_size": {"value": 0.0, "unit": "deg"},
                "weighting": "WEIGHTING",
            }
        ],
        data_product_src_nets=[{"data_products_src_id": "2"}],
        result_details=[
            {
                "observation_set_ref": "low-002",
                "target_ref": TEST_TARGET_ID,
                "result": {
                    "supplied_type": "integration_time",
                    "weighted_continuum_sensitivity": {"value": 0.0, "unit": "Hz"},
                    "weighted_spectral_sensitivity": {"value": 0.0, "unit": "Hz"},
                    "total_continuum_sensitivity": {"value": 0.0, "unit": "Hz"},
                    "total_spectral_sensitivity": {"value": 0.0, "unit": "Hz"},
                    "surface_brightness_sensitivity": {
                        "continuum": 0.0,
                        "spectral": 0.0,
                        "unit": "Hz",
                    },
                },
                "continuum_confusion_noise": {"value": 0.0, "unit": "Hz"},
                "synthesized_beam_size": {
                    "continuum": "0.0",
                    "spectral": "0.0",
                    "unit": "Hz",
                },
                "spectral_confusion_noise": {"value": 0.0, "unit": "Hz"},
            }
        ],
    ),
)


PROPOSAL_DRAFT_JSON = load_string_from_file("testfile_sample_proposal_draft.json")

PROPOSAL_DRAFT = Proposal(
    prsl_id="prp-ska01-202204-02",
    status="draft",
    submitted_on=None,
    submitted_by=None,
    investigator_refs=[
        "TestUser",
    ],
    metadata=METADATA,
    cycle="SKA_5000_2023",
    info=Info(
        title="The Milky Way View",
        abstract="",
        proposal_type=ProposalType(
            main_type="standard_proposal", attributes=["coordinated_proposal"]
        ),
        science_category="",
        targets=[],
        documents=[],
        investigators=[
            {
                "investigator_id": "TestUser",
                "given_name": "Tony",
                "family_name": "Bennet",
                "email": "somewhere.vague@example.com",
                "organization": "",
                "for_phd": False,
                "status": "pending",
                "principal_investigator": True,
            }
        ],
        observation_sets=[],
        data_product_sdps=[],
        data_product_src_nets=[],
        result_details=[],
    ),
)


TEST_CASES = (
    pytest.param(
        PROPOSAL_SUBMITTED_JSON,
        PROPOSAL_SUBMITTED.model_copy,
        id=lbl("proposal_submitted"),
    ),
    pytest.param(
        PROPOSAL_DRAFT_JSON, PROPOSAL_DRAFT.model_copy, id=lbl("proposal_draft")
    ),
)
