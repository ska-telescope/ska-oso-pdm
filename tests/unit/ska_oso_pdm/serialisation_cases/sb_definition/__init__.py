from itertools import chain

from .procedures import TEST_CASES as procedures_cases
from .sb_definition import TEST_CASES as sbd_cases
from .scan_definition import TEST_CASES as scandefinition_cases
from .target import TEST_CASES as target_cases

TEST_CASES = tuple(
    chain(procedures_cases, sbd_cases, scandefinition_cases, target_cases)
)
