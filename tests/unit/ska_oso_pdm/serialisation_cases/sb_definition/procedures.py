"""
tests for the procedures_schema to validate the
conversion between the JSON and Python representations
of the activities section of an SKA Scheduling Block
"""

import json

import pytest

from ska_oso_pdm.sb_definition.procedures import (
    FilesystemScript,
    GitScript,
    InlineScript,
    PythonArguments,
)
from tests.unit.ska_oso_pdm.utils import lbl

INLINE_SCRIPT_JSON = {
    "kind": "inline",
    "function_args": {
        "first": {
            "args": ["one arg"],
        }
    },
    "content": "the script content",
}


INLINE_SCRIPT = InlineScript(
    content="the script content",
    function_args={"first": PythonArguments(args=["one arg"])},
)


FILESYSTEM_SCRIPT_JSON = {
    "kind": "filesystem",
    "function_args": {"first": {"kwargs": {"one": "kwarg"}}},
    "path": "/path/to/script.py",
}

FILESYSTEM_SCRIPT = FilesystemScript(
    path="/path/to/script.py",
    function_args={"first": PythonArguments(kwargs={"one": "kwarg"})},
)

GIT_SCRIPT_JSON = {
    "kind": "git",
    "function_args": {"first": {"kwargs": {"one": "kwarg"}}},
    "path": "/path/to/script.py",
    "repo": "http://repo",
    "branch": "main",
    "commit": "dbf8447fee57581a722e3d24582ca14cbba1f90f",
}


GIT_SCRIPT = GitScript(
    path="/path/to/script.py",
    repo="http://repo",
    branch="main",
    commit="dbf8447fee57581a722e3d24582ca14cbba1f90f",
    function_args={"first": PythonArguments(kwargs={"one": "kwarg"})},
)

TEST_CASES = (
    pytest.param(
        json.dumps(INLINE_SCRIPT_JSON),
        INLINE_SCRIPT.model_copy,
        id=lbl("inline_script"),
    ),
    pytest.param(
        json.dumps(FILESYSTEM_SCRIPT_JSON),
        FILESYSTEM_SCRIPT.model_copy,
        id=lbl("filesystem_script"),
    ),
    pytest.param(
        json.dumps(GIT_SCRIPT_JSON), GIT_SCRIPT.model_copy, id=lbl("git_script")
    ),
)
