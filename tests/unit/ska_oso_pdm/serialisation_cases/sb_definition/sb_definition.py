"""
tests for the scheduling_block_schema to validate the
conversion between the JSON and Python representations
of an SKA Scheduling Block
"""

from datetime import datetime, timedelta, timezone

import pytest
from astropy.units import Quantity

from ska_oso_pdm._shared import Metadata
from ska_oso_pdm._shared.target import (
    Beam,
    EquatorialCoordinates,
    EquatorialCoordinatesPST,
    FivePointParameters,
    PointingKind,
    PointingPattern,
    RadialVelocity,
    RadialVelocityDefinition,
    RadialVelocityReferenceFrame,
    RadialVelocityUnits,
    RasterParameters,
    SinglePointParameters,
    SolarSystemObject,
    SpiralParameters,
    StarRasterParameters,
    Target,
    TiedArrayBeams,
)
from ska_oso_pdm.builders import DISH_ALLOCATION_ID, low_imaging_sb, mid_imaging_sb
from ska_oso_pdm.sb_definition.csp.cbf import (
    CBFConfiguration,
    FSPConfiguration,
    FSPFunctionMode,
)
from ska_oso_pdm.sb_definition.csp.csp_configuration import CSPConfiguration
from ska_oso_pdm.sb_definition.csp.subarray import SubarrayConfiguration
from ska_oso_pdm.sb_definition.dish.dish_allocation import DishAllocation
from ska_oso_pdm.sb_definition.procedures import (
    FilesystemScript,
    GitScript,
    PythonArguments,
)
from ska_oso_pdm.sb_definition.sb_definition import SBDefinition, TelescopeType
from ska_oso_pdm.sb_definition.scan_definition import PointingCorrection, ScanDefinition
from tests.unit.ska_oso_pdm.serialisation_cases.csp.common import (
    CommonConfigurationFactory,
)
from tests.unit.ska_oso_pdm.serialisation_cases.csp.csp_configuration import (
    LOW_CSP_CONFIG,
    MID_CSP_CONFIG,
)
from tests.unit.ska_oso_pdm.serialisation_cases.mccs.mccs_allocation import (
    MCCS_ALLOCATION,
)
from tests.unit.ska_oso_pdm.serialisation_cases.sdp.sdp_configuration import (
    SDPConfigurationFactory,
    holography_sdp_configuration,
)
from tests.unit.ska_oso_pdm.utils import (
    TEST_CSP_CONFIGURATION_ID,
    TEST_SCAN_DEFINITION_ID,
    TEST_TARGET_ID,
    lbl,
    load_string_from_file,
)

METADATA_JSON = """
    {
        "created_by": "Liz Bartlett",
        "created_on": "2022-04-27T23:20:47.922400Z",
        "last_modified_on": "2023-04-27T23:20:47.922400Z",
        "version": 1,
        "pdm_version": "15.3.0"
    }
"""
DEFAULT_FUNCTION_ARGS = {
    "init": PythonArguments(args=["posarg1", "posarg2"], kwargs={"argname": "argval"}),
    "main": PythonArguments(args=["posarg1", "posarg2"], kwargs={"argname": "argval"}),
}


def metadata_instance():
    return Metadata(
        created_by="Liz Bartlett",
        version=1,
        created_on=datetime(2022, 4, 27, 23, 20, 47, 922400, tzinfo=timezone.utc),
        last_modified_on=datetime(2023, 4, 27, 23, 20, 47, 922400, tzinfo=timezone.utc),
        pdm_version="15.3.0",
    )


def create_test_mid_sbdefinition():
    """
    Utility method to create a valid MID sb configuration for use in unit test
    """
    sbd = SBDefinition(
        sbd_id="sbi-mvp01-20200325-00001",
        telescope=TelescopeType.SKA_MID,
        metadata=metadata_instance(),
        activities={
            "allocate": FilesystemScript(
                path="file:///path/to/allocatescript.py",
                function_args=DEFAULT_FUNCTION_ARGS,
            ),
            "observe": GitScript(
                repo="https://gitlab.com/script_repo/operational_scripts",
                path="git://relative/path/to/scriptinsiderepo.py",
                branch="main",
                function_args=DEFAULT_FUNCTION_ARGS,
            ),
        },
    )

    # Add metadata
    sbd.metadata.created_on = datetime(
        2022, 3, 28, 15, 43, 53, 971548, tzinfo=timezone.utc
    )
    sbd.metadata.created_by = "Liz Bartlett"
    sbd.metadata.last_modified_on = datetime(
        2022, 3, 28, 15, 43, 53, 971548, tzinfo=timezone.utc
    )
    sbd.metadata.last_modified_by = "Liz Bartlett"

    csp_config = MID_CSP_CONFIG.model_copy()
    sbd.csp_configurations = [csp_config]

    calibrator_target = Target(
        target_id=TEST_TARGET_ID,
        name="Polaris Australis",
        reference_coordinate=EquatorialCoordinates(ra="21:08:47.92", dec="-88:57:22.9"),
        pointing_pattern=PointingPattern(
            active=PointingKind.FIVE_POINT,
            parameters=[
                FivePointParameters(offset_arcsec=5.0),
                RasterParameters(
                    row_length_arcsec=1.23,
                    row_offset_arcsec=4.56,
                    n_rows=2,
                    pa=7.89,
                    unidirectional=True,
                ),
                StarRasterParameters(
                    row_length_arcsec=1.23,
                    n_rows=2,
                    row_offset_angle=4.56,
                    unidirectional=True,
                ),
            ],
        ),
        radial_velocity=RadialVelocity(
            quantity=Quantity(value=-12.345, unit=RadialVelocityUnits.KM_PER_SEC),
            definition=RadialVelocityDefinition.OPTICAL,
            reference_frame=RadialVelocityReferenceFrame.LSRK,
            redshift=1.23,
        ),
        tied_array_beams=TiedArrayBeams(
            pst_beams=[
                Beam(
                    beam_id=1,
                    beam_coordinate=EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204R",
                        reference_frame="icrs",
                        ra_str="00:24:05.670",
                        dec_str="-72:04:52.62",
                        pm_ra=4.8,
                        pm_dec=-3.3,
                    ),
                    stn_weights=[1.0, 1.0],
                ),
                Beam(
                    beam_id=2,
                    beam_coordinate=EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204W",
                        reference_frame="icrs",
                        ra_str="00:24:06.0580",
                        dec_str="-72:04:49.088",
                        pm_ra=6.1,
                        pm_dec=-2.6,
                    ),
                    stn_weights=[1.0, 1.0],
                ),
            ],
            pss_beams=[],
            vlbi_beams=[],
        ),
    )

    science_target = Target(
        target_id="target-67890",
        name="M83",
        reference_coordinate=EquatorialCoordinates(
            ra="13:37:00.919", dec="-29:51:56.74"
        ),
        pointing_pattern=PointingPattern(
            active=PointingKind.SINGLE_POINT, parameters=[SinglePointParameters()]
        ),
        radial_velocity=RadialVelocity(),
        tied_array_beams=TiedArrayBeams(
            pst_beams=[
                Beam(
                    beam_id=1,
                    beam_coordinate=EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204R",
                        reference_frame="icrs",
                        ra_str="00:24:05.670",
                        dec_str="-72:04:52.62",
                        pm_ra=4.8,
                        pm_dec=-3.3,
                    ),
                    stn_weights=[1.0, 1.0],
                ),
                Beam(
                    beam_id=2,
                    beam_coordinate=EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204W",
                        reference_frame="icrs",
                        ra_str="00:24:06.0580",
                        dec_str="-72:04:49.088",
                        pm_ra=6.1,
                        pm_dec=-2.6,
                    ),
                    stn_weights=[1.0, 1.0],
                ),
            ],
            pss_beams=[],
            vlbi_beams=[],
        ),
    )

    sbd.targets = [calibrator_target, science_target]

    # Add dish_allocation
    dish_allocation = DishAllocation(
        dish_allocation_id=DISH_ALLOCATION_ID,
        dish_ids=frozenset(["SKA100", "SKA001", "SKA063", "SKA036"]),
        selected_subarray_definition="AA0.5",
    )
    sbd.dish_allocations = dish_allocation

    # Add scan_definitions
    calibrator_scan = ScanDefinition(
        scan_definition_id="calibrator scan",
        scan_duration_ms=timedelta(seconds=60),
        csp_configuration_ref=csp_config.config_id,
        target_ref=calibrator_target.target_id,
        dish_allocation_ref=dish_allocation.dish_allocation_id,
        scan_type_ref="calibration_B",
    )
    science_scan = ScanDefinition(
        scan_definition_id="science scan",
        scan_duration_ms=timedelta(seconds=60),
        csp_configuration_ref=csp_config.config_id,
        target_ref=science_target.target_id,
        dish_allocation_ref=dish_allocation.dish_allocation_id,
        scan_type_ref="science_A",
        pointing_correction=PointingCorrection.UPDATE,
    )

    sbd.scan_definitions.append(calibrator_scan)
    sbd.scan_definitions.append(science_scan)

    sbd.scan_sequence = [
        calibrator_scan.scan_definition_id,
        science_scan.scan_definition_id,
        science_scan.scan_definition_id,
        calibrator_scan.scan_definition_id,
    ]

    sbd.sdp_configuration = SDPConfigurationFactory()

    return sbd


def create_test_low_sbdefinition():
    """
    Utility method to create a valid LOW sb configuration for use in unit test
    """

    low_sbd = SBDefinition(
        sbd_id="sbi-mvp01-20200325-00001",
        telescope=TelescopeType.SKA_LOW,
        metadata=metadata_instance(),
        activities={
            "allocate": FilesystemScript(
                path="/path/to/allocatescript.py",
                function_args=DEFAULT_FUNCTION_ARGS,
            ),
            "observe": GitScript(
                repo="https://gitlab.com/script_repo/operational_scripts",
                path="/relative/path/to/scriptinsiderepo.py",
                branch="main",
                commit="d234c257dadd18b3edcd990b8194c6ad94fc278a",
                function_args=DEFAULT_FUNCTION_ARGS,
            ),
        },
    )

    low_sbd.metadata.created_on = datetime(
        2022, 3, 28, 15, 43, 53, 971548, tzinfo=timezone.utc
    )
    low_sbd.metadata.last_modified_on = datetime(
        2024, 7, 15, 21, 55, 9, 654185, tzinfo=timezone.utc
    )

    low_sbd.targets = [
        Target(
            target_id=TEST_TARGET_ID,
            name="47 Tuc",
            pointing_pattern=PointingPattern(
                active=PointingKind.SINGLE_POINT, parameters=[SinglePointParameters()]
            ),
            reference_coordinate=EquatorialCoordinates(
                ra="00:24:05.359", dec="-72:04:53.20"
            ),
            radial_velocity=RadialVelocity(
                quantity=Quantity(value=-17.2, unit=RadialVelocityUnits.KM_PER_SEC),
                definition=RadialVelocityDefinition.OPTICAL,
                reference_frame=RadialVelocityReferenceFrame.BARYCENTRIC,
                redshift=0.0,
            ),
            tied_array_beams=TiedArrayBeams(
                pst_beams=[
                    Beam(
                        beam_id=1,
                        beam_coordinate=EquatorialCoordinatesPST(
                            target_id="PSR J0024-7204R",
                            reference_frame="icrs",
                            ra_str="00:24:05.670",
                            dec_str="-72:04:52.62",
                            pm_ra=4.8,
                            pm_dec=-3.3,
                        ),
                        stn_weights=[1.0, 1.0],
                    ),
                    Beam(
                        beam_id=2,
                        beam_coordinate=EquatorialCoordinatesPST(
                            target_id="PSR J0024-7204W",
                            reference_frame="icrs",
                            ra_str="00:24:06.0580",
                            dec_str="-72:04:49.088",
                            pm_ra=6.1,
                            pm_dec=-2.6,
                        ),
                        stn_weights=[1.0, 1.0],
                    ),
                ],
                pss_beams=[],
                vlbi_beams=[],
            ),
        )
    ]

    science_scan = ScanDefinition(
        scan_definition_id="scan 1",
        scan_duration_ms=timedelta(seconds=64.0),
        target_ref=TEST_TARGET_ID,
        mccs_allocation_ref="mccs-allocation-74519",
        csp_configuration_ref=TEST_CSP_CONFIGURATION_ID,
        scan_intent="Science",
    )

    low_sbd.scan_definitions = [science_scan]
    low_sbd.scan_sequence = [science_scan.scan_definition_id]
    low_sbd.mccs_allocation = MCCS_ALLOCATION
    low_sbd.csp_configurations = [LOW_CSP_CONFIG.model_copy()]
    low_sbd.csp_configurations[0].lowcbf.do_pst = True

    return low_sbd


def create_test_holography_mid_sbdefinition() -> SBDefinition:
    """
    Utility method to create a valid LOW sb configuration for use in unit test
    """

    holography_sbd = SBDefinition(
        sbd_id="sbi-mvp01-20240812-00001",
        telescope=TelescopeType.SKA_MID,
        metadata=Metadata(
            created_by="Andy Biggs",
            version=1,
            created_on=datetime(2024, 7, 11, 15, 43, 53, 971548, tzinfo=timezone.utc),
            last_modified_on=datetime(
                2024, 7, 11, 15, 43, 53, 971548, tzinfo=timezone.utc
            ),
            last_modified_by="Andy Biggs",
            pdm_version="15.3.0",
        ),
        activities={
            "observe": GitScript(
                path="git://scripts/test_example_mid_script.py",
                repo="https://gitlab.com/ska-telescope/oso/ska-oso-scripting",
                branch="master",
                function_args={
                    "init": PythonArguments(args=[], kwargs={"subarray_id": 1}),
                    "main": PythonArguments(args=[], kwargs={}),
                },
            ),
        },
    )

    holography_sbd.targets = [
        Target(
            target_id=TEST_TARGET_ID,
            name="Mars",
            pointing_pattern=PointingPattern(
                active=PointingKind.SPIRAL,
                parameters=[
                    SpiralParameters(
                        kind=PointingKind.SPIRAL,
                        scan_extent=Quantity(value=5.0, unit="deg"),
                        track_time=Quantity(value=30.0, unit="s"),
                        cycle_track_time=Quantity(value=10.0, unit="s"),
                        slow_time=Quantity(value=6.0, unit="s"),
                        sample_time=Quantity(value=0.25, unit="s"),
                        scan_speed=Quantity(value=0.1, unit="deg / s"),
                        slew_speed=Quantity(value=-1.0, unit="deg / s"),
                        twist_factor=0.5,
                        high_el_slowdown_factor=2.0,
                    )
                ],
            ),
            reference_coordinate=SolarSystemObject(name="Mars"),
        )
    ]

    scan = ScanDefinition(
        scan_definition_id="calibrator_scan",
        scan_duration_ms=timedelta(seconds=60.0),
        target_ref=TEST_TARGET_ID,
        dish_allocation_ref=DISH_ALLOCATION_ID,
        scan_type_ref="calibration_B",
        csp_configuration_ref=TEST_CSP_CONFIGURATION_ID,
    )

    holography_sbd.scan_definitions = [scan]
    holography_sbd.scan_sequence = ["calibrator_scan", "calibrator_scan"]

    csp_item = CSPConfiguration(
        config_id=TEST_CSP_CONFIGURATION_ID,
        name="csp config 123",
        subarray=SubarrayConfiguration(subarray_name="science period 23"),
        common=CommonConfigurationFactory(),
        cbf=CBFConfiguration(
            fsps=[
                FSPConfiguration(
                    fsp_id=1,
                    function_mode=FSPFunctionMode.CORR,
                    frequency_slice_id=1,
                    integration_factor=1,
                    zoom_factor=0,
                    channel_averaging_map=[(0, 2), (744, 0)],
                    channel_offset=0,
                    output_link_map=[(0, 0), (200, 1)],
                )
            ]
        ),
    )

    holography_sbd.csp_configurations = [csp_item]

    dish_allocation = DishAllocation(
        dish_allocation_id=DISH_ALLOCATION_ID,
        dish_ids=frozenset(["SKA001", "SKA036", "SKA063", "SKA100"]),
    )
    holography_sbd.dish_allocations = dish_allocation
    holography_sbd.sdp_configuration = holography_sdp_configuration()

    return holography_sbd


MID_SB_JSON = load_string_from_file("testfile_sample_mid_sb.json")
LOW_SB_JSON = load_string_from_file("testfile_sample_low_sb.json")
MID_HOLOGRAPHY_SB_JSON = load_string_from_file("testfile_holography_mid_sb.json")

MINIMAL_SB_JSON = """
{
  "telescope": "ska_mid",
  "interface": "https://schema.skao.int/ska-oso-pdm-sbd/0.1"
}
"""

LOW_IMAGING_SB_JSON = load_string_from_file("low_imaging_sb.json")
MID_IMAGING_SB_JSON = load_string_from_file("mid_imaging_sb.json")


def create_test_low_imaging_sb():
    """
    Utility method to create a valid Low SB Definition object using the builders.

    The builders will create the SBD Metadata with the current timestamp which will
    not be reflected in the compared JSON, so we omit it for now as a workaround.
    """
    low_sbd = low_imaging_sb()
    set_test_internal_ids(low_sbd)

    # This workaround is needed for now because the Metadata object is created with
    # functools.partial which means the @freezegun.freeze_time decorator does not
    # work when using the builder function.
    low_sbd.metadata.created_on = datetime.now(tz=timezone.utc)
    low_sbd.metadata.last_modified_on = datetime.now(tz=timezone.utc)
    return low_sbd


def create_test_mid_imaging_sb():
    """
    Utility method to create a valid Mid SB Definition object using the builders.

    The builders will create the SBD Metadata with the current timestamp which will
    not be reflected in the compared JSON, so we omit it for now as a workaround.
    """
    mid_sbd = mid_imaging_sb(dishes=["SKA001", "SKA036", "SKA063", "SKA100"])

    set_test_internal_ids(mid_sbd)

    # This workaround is needed for now because the Metadata object is created with
    # functools.partial which means the @freezegun.freeze_time decorator does not
    # work when using the builder function.
    mid_sbd.metadata.created_on = datetime.now(tz=timezone.utc)
    mid_sbd.metadata.last_modified_on = datetime.now(tz=timezone.utc)
    return mid_sbd


def minimal_sb_instance():
    return SBDefinition(
        telescope=TelescopeType.SKA_MID,
    )


def set_test_internal_ids(sbd: SBDefinition) -> None:
    # As the id_key function is called in several places in different layers of the builder,
    # it is difficult to mock in the tests, so we just override the ids here.
    sbd.targets[0].target_id = TEST_TARGET_ID
    sbd.scan_definitions[0].target_ref = TEST_TARGET_ID
    sbd.csp_configurations[0].config_id = TEST_CSP_CONFIGURATION_ID
    sbd.scan_definitions[0].csp_configuration_ref = TEST_CSP_CONFIGURATION_ID
    sbd.scan_definitions[0].scan_definition_id = TEST_SCAN_DEFINITION_ID
    sbd.scan_sequence = [TEST_SCAN_DEFINITION_ID]


TEST_CASES = (
    pytest.param(METADATA_JSON, metadata_instance, id=lbl("metadata")),
    pytest.param(MINIMAL_SB_JSON, minimal_sb_instance, id=lbl("minimal_sb")),
    pytest.param(
        MID_SB_JSON,
        create_test_mid_sbdefinition,
        id=lbl("mid_sb"),
    ),
    pytest.param(
        MID_IMAGING_SB_JSON,
        create_test_mid_imaging_sb,
        id=lbl("mid_imaging_sb"),
    ),
    pytest.param(
        LOW_SB_JSON,
        create_test_low_sbdefinition,
        id=lbl("low_sb"),
    ),
    pytest.param(
        LOW_IMAGING_SB_JSON,
        create_test_low_imaging_sb,
        id=lbl("low_imaging_sb"),
    ),
    pytest.param(
        MID_HOLOGRAPHY_SB_JSON,
        create_test_holography_mid_sbdefinition,
        id=lbl("mid_holography_sb"),
    ),
)
