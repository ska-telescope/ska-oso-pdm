"""
tests for the scan_definitions_schema to validate the
conversion between the JSON and Python representations
of the scan definitions section of an SKA Scheduling Block
"""

from datetime import timedelta

import pytest

from ska_oso_pdm.sb_definition.scan_definition import ScanDefinition
from tests.unit.ska_oso_pdm.utils import lbl

MID_SCANDEFINITION = ScanDefinition(
    scan_definition_id="id_42",
    scan_duration_ms=timedelta(hours=1.0),
    scan_type_ref="science",
    dish_allocation_ref="dish config 123",
    target_ref="m51",
    csp_configuration_ref="csp_id_41",
)

MID_SCANDEFINITION_JSON = """
{
    "scan_definition_id": "id_42",
    "scan_duration_ms": 3600000.0,
    "target_ref": "m51",
    "csp_configuration_ref": "csp_id_41",
    "dish_allocation_ref": "dish config 123",
    "scan_type_ref": "science",
    "pointing_correction": "MAINTAIN"
}
"""

LOW_SCANDEFINITION = ScanDefinition(
    scan_definition_id="id_43",
    scan_duration_ms=timedelta(seconds=12.3),
    target_ref="47 Tuc",
    mccs_allocation_ref="mccs-allocation-74519",
    csp_configuration_ref="csp-mvp01-20220329-00001",
)

LOW_SCANDEFINITION_JSON = """
{
    "scan_definition_id": "id_43",
    "scan_duration_ms": 12300.0,
    "target_ref": "47 Tuc",
    "mccs_allocation_ref": "mccs-allocation-74519",
    "csp_configuration_ref": "csp-mvp01-20220329-00001",
    "pointing_correction": "MAINTAIN"
}
"""

MINIMAL_SCANDEFINITION = ScanDefinition(
    scan_definition_id="id_42", scan_duration_ms=timedelta(minutes=30)
)

MINIMAL_SCANDEFINITION_JSON = """
{
    "scan_definition_id": "id_42",
    "scan_duration_ms": 1800000,
    "pointing_correction": "MAINTAIN"
}
"""

TEST_CASES = (
    pytest.param(
        MID_SCANDEFINITION_JSON,
        MID_SCANDEFINITION.model_copy,
        id=lbl("mid_scandefinition"),
    ),
    pytest.param(
        LOW_SCANDEFINITION_JSON,
        LOW_SCANDEFINITION.model_copy,
        id=lbl("low_scandefinition"),
    ),
    pytest.param(
        MINIMAL_SCANDEFINITION_JSON,
        MINIMAL_SCANDEFINITION.model_copy,
        id=lbl("minimal_scandefinition"),
    ),
)
