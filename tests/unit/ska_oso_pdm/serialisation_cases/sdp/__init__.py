from itertools import chain

from .beam import TEST_CASES as beam_cases
from .beammapping_and_resources import TEST_CASES as beammapping_cases
from .channels import TEST_CASES as channels
from .execution_block import TEST_CASES as execution_block_cases
from .polarisation import TEST_CASES as polarisation_cases
from .processing_block import TEST_CASES as processing_block_cases
from .sdp_configuration import TEST_CASES as sdb_configuration_cases

TEST_CASES = tuple(
    chain(
        beam_cases,
        beammapping_cases,
        channels,
        execution_block_cases,
        polarisation_cases,
        processing_block_cases,
        sdb_configuration_cases,
    )
)
