"""
Tests for the SDP Beam schema to validate the
conversion between the JSON and Python representations
of the beams section of an SDP configuration
"""

import pytest

from ska_oso_pdm.sb_definition.sdp.beam import Beam, BeamFunction
from tests.unit.ska_oso_pdm.utils import lbl

VALID_VIS_BEAM_INSTANCE = Beam(beam_id="vis", function=BeamFunction.VISIBILITIES)
VALID_VIS_BEAM_JSON = """
{
  "beam_id": "vis",
  "function": "visibilities"
}
"""

VALID_PST_BEAM_INSTANCE = Beam(
    beam_id="pst", function=BeamFunction.PULSAR_TIMING, timing_beam_id=123
)
VALID_PST_BEAM_JSON = """
{
  "beam_id": "pst",
  "function": "pulsar timing",
  "timing_beam_id": 123
}
"""

VALID_PSS_BEAM_INSTANCE = Beam(
    beam_id="pss", function=BeamFunction.PULSAR_SEARCH, search_beam_id=123
)
VALID_PSS_BEAM_JSON = """
{
  "beam_id": "pss",
  "function": "pulsar search",
  "search_beam_id": 123
}
"""

VALID_VLBI_BEAM_INSTANCE = Beam(
    beam_id="vlbi", function=BeamFunction.VLBI, vlbi_beam_id=123
)
VALID_VLBI_BEAM_JSON = """
{
  "beam_id": "vlbi",
  "function": "vlbi",
  "vlbi_beam_id": 123
}
"""

TEST_CASES = (
    pytest.param(
        VALID_VIS_BEAM_JSON, VALID_VIS_BEAM_INSTANCE.model_copy, id=lbl("beam_vis")
    ),
    pytest.param(
        VALID_PST_BEAM_JSON, VALID_PST_BEAM_INSTANCE.model_copy, id=lbl("beam_pst")
    ),
    pytest.param(
        VALID_PSS_BEAM_JSON, VALID_PSS_BEAM_INSTANCE.model_copy, id=lbl("beam_pss")
    ),
    pytest.param(
        VALID_VLBI_BEAM_JSON, VALID_VLBI_BEAM_INSTANCE.model_copy, id=lbl("beam_vlbi")
    ),
)
