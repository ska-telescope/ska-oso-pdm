"""
tests for the scan_type schema to validate the
conversion between the JSON and Python representations
of the scan type section of an SDP configuration
"""

import itertools

import pytest

from ska_oso_pdm.sb_definition.sdp.resources import Resources
from ska_oso_pdm.sb_definition.sdp.scan_type import BeamMapping, ScanType
from tests.unit.ska_oso_pdm.utils import lbl

VALID_BEAMMAPPING = BeamMapping(
    beam_ref="pss1",
    field_ref="pss_field_0",
    channels_ref="pulsar_channels",
    polarisations_ref="all",
)
VALID_BEAMMAPPING_JSON = """
{
  "beam_ref": "pss1",
  "field_ref": "pss_field_0",
  "polarisations_ref": "all",
  "channels_ref": "pulsar_channels"
}
"""

VALID_MINIMAL_BEAMMAPPING = BeamMapping(beam_ref="vis0")
VALID_MINIMAL_BEAMMAPPING_JSON = """
{
  "beam_ref": "vis0"
}
"""
VALID_MINIMAL_BEAMMAPPING_JSON_WITH_NULLS = """
{
  "beam_ref": "vis0",
  "field_ref": null,
  "polarisations_ref": null,
  "channels_ref": null
}
"""

beam_mapping_cases = (
    pytest.param(
        VALID_BEAMMAPPING_JSON, VALID_BEAMMAPPING.model_copy, id=lbl("beammapping")
    ),
    pytest.param(
        VALID_MINIMAL_BEAMMAPPING_JSON,
        VALID_MINIMAL_BEAMMAPPING.model_copy,
        id=lbl("minimal_beammapping"),
    ),
)


VALID_SCANTYPE = ScanType(
    scan_type_id=".default",
    derive_from="foo",
    beams=[
        BeamMapping(
            beam_ref="pss1",
            field_ref="pss_field_0",
            channels_ref="pulsar_channels",
            polarisations_ref="all",
        ),
        BeamMapping(
            beam_ref="pst1",
            field_ref="pst_field_0",
            channels_ref="pulsar_channels",
            polarisations_ref="all",
        ),
    ],
)

VALID_SCANTYPE_JSON = """
{
  "scan_type_id": ".default",
  "derive_from": "foo",
  "beams": [
  {
    "polarisations_ref": "all",
    "beam_ref": "pss1",
    "channels_ref": "pulsar_channels",
    "field_ref": "pss_field_0"
  },
  {
    "polarisations_ref": "all",
    "beam_ref": "pst1",
    "channels_ref": "pulsar_channels",
    "field_ref": "pst_field_0"
    }
  ]
}
"""

VALID_EMPTY_SCANTYPE = ScanType(scan_type_id=".default")
VALID_EMPTY_SCANTYPE_JSON = """
{
  "scan_type_id": ".default"
}
"""

scan_type_cases = (
    pytest.param(VALID_SCANTYPE_JSON, VALID_SCANTYPE.model_copy, id=lbl("scan_type")),
    pytest.param(
        VALID_EMPTY_SCANTYPE_JSON,
        VALID_EMPTY_SCANTYPE.model_copy,
        id=lbl("empty_scan_type"),
    ),
)


VALID_RESOURCES = Resources(
    csp_links=[1, 2, 3, 4],
    receptors=["FS4", "FS8", "FS16", "FS17"],
    receive_nodes=10,
)

VALID_RESOURCES_JSON = """
{
  "csp_links": [1, 2, 3, 4],
  "receptors": ["FS4", "FS8", "FS16", "FS17"],
  "receive_nodes": 10
}
"""

resource_cases = (
    pytest.param(VALID_RESOURCES_JSON, VALID_RESOURCES.model_copy, id=lbl("resources")),
    pytest.param("{}", Resources, id=lbl("empty_resources")),
)

TEST_CASES = tuple(itertools.chain(scan_type_cases, beam_mapping_cases, resource_cases))
