"""
Tests for the channels schema to validate the
conversion between the JSON and Python representations
of the channels section of an SDP configuration
"""

from functools import partial

import pytest

from ska_oso_pdm.sb_definition.sdp.channels import Channels, SpectralWindow
from tests.unit.ska_oso_pdm.utils import lbl

SpectralWindowFactory = partial(
    SpectralWindow,
    spectral_window_id="fsp_1_channels",
    count=744,
    start=0,
    stride=2,
    freq_min=350000000.0,
    freq_max=368000000.0,
    link_map=[(0, 0), (200, 1), (744, 2), (944, 3)],
)

SPECTRALWINDOW_JSON = """
{
  "spectral_window_id": "fsp_1_channels",
  "count": 744,
  "start": 0,
  "stride": 2,
  "freq_min": 350000000.0,
  "freq_max": 368000000.0,
  "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]]
}
"""


MINIMAL_SPECTRALWINDOW = SpectralWindow(
    spectral_window_id="foo", count=744, start=0, freq_min=123.0, freq_max=456.0
)

MINIMAL_SPECTRALWINDOW_JSON = """
{
  "spectral_window_id": "foo",
  "count": 744,
  "start": 0,
  "freq_min": 123.0,
  "freq_max": 456.0
}
"""


ChannelsFactory = partial(
    Channels,
    channels_id="pulsar_channels",
    spectral_windows=[
        SpectralWindow(
            spectral_window_id="pulsar_fsp_channels",
            count=744,
            start=0,
            freq_min=350000000.0,
            freq_max=368000000.0,
        )
    ],
)

CHANNELS_JSON = """
{
  "channels_id": "pulsar_channels",
  "spectral_windows": [{
    "spectral_window_id": "pulsar_fsp_channels",
    "count": 744,
    "start": 0,
    "freq_min": 350000000.0,
    "freq_max": 368000000.0
  }]
}
"""

TEST_CASES = (
    pytest.param(
        SPECTRALWINDOW_JSON,
        SpectralWindowFactory,
        id=lbl("spectralwindow"),
    ),
    pytest.param(
        MINIMAL_SPECTRALWINDOW_JSON,
        MINIMAL_SPECTRALWINDOW.model_copy,
        id=lbl("spectralwindow_minimal"),
    ),
    pytest.param(CHANNELS_JSON, ChannelsFactory, id=lbl("channels")),
)
