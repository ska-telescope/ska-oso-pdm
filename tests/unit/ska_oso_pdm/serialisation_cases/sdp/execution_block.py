"""
tests for the sdp_configuration schema to validate the
conversion between the JSON and Python representations
of the SDP configuration for an SKA Scheduling Block
"""

import functools
import json

import pytest

from ska_oso_pdm.sb_definition.sdp.beam import Beam, BeamFunction
from ska_oso_pdm.sb_definition.sdp.channels import Channels, SpectralWindow
from ska_oso_pdm.sb_definition.sdp.execution_block import ExecutionBlock
from ska_oso_pdm.sb_definition.sdp.polarisation import Polarisation
from ska_oso_pdm.sb_definition.sdp.scan_type import BeamMapping, ScanType
from tests.unit.ska_oso_pdm.utils import lbl


def ExecutionBlockFactory(**overrides) -> ExecutionBlock:
    """
    Creates a test Execution Block for use in tests.
    """
    # beams info

    beam1 = Beam(beam_id="vis0", function=BeamFunction.VISIBILITIES)
    beam2 = Beam(beam_id="pst1", timing_beam_id=1, function=BeamFunction.PULSAR_TIMING)
    beam3 = Beam(beam_id="pst2", timing_beam_id=2, function=BeamFunction.PULSAR_TIMING)

    beams = [beam1, beam2, beam3]

    #  Scan_types info
    beam_mapping_1 = BeamMapping(
        beam_ref="vis0", channels_ref="vis_channels", polarisations_ref="all"
    )
    beam_mapping_2 = BeamMapping(
        beam_ref="pst1",
        field_ref="M83",
        channels_ref="pulsar_channels",
        polarisations_ref="all",
    )

    beam_mapping_3 = BeamMapping(
        beam_ref="pst2",
        field_ref="Polaris Australis",
        channels_ref="pulsar_channels",
        polarisations_ref="all",
    )

    beam_mapping_4 = BeamMapping(
        beam_ref="vis0",
        field_ref="M83",
    )

    scan_type_1 = ScanType(
        scan_type_id=".default",
        beams=[
            beam_mapping_1,
            beam_mapping_2,
            beam_mapping_3,
        ],
    )

    scan_type_2 = ScanType(
        scan_type_id=".default", beams=[beam_mapping_4], derive_from=".default"
    )
    scan_types = [scan_type_1, scan_type_2]

    #  Channels kwarg.

    spectral_window_1 = SpectralWindow(
        spectral_window_id="fsp_1_channels",
        count=744,
        start=0,
        freq_min=0.35e9,
        freq_max=0.368e9,
        stride=2,
        link_map=[(0, 0), (200, 1), (744, 2), (944, 3)],
    )
    spectral_window_2 = SpectralWindow(
        spectral_window_id="fsp_2_channels",
        count=744,
        start=2000,
        freq_min=0.36e9,
        freq_max=0.368e9,
        stride=1,
        link_map=[(2000, 4), (2200, 5)],
    )

    spectral_window_3 = SpectralWindow(
        spectral_window_id="zoom_window_1",
        count=744,
        start=4000,
        freq_min=0.36e9,
        freq_max=0.361e9,
        stride=1,
        link_map=[(4000, 6), (4200, 7)],
    )

    spectral_window_4 = SpectralWindow(
        spectral_window_id="pulsar_fsp_channels",
        count=744,
        start=0,
        freq_min=0.35e9,
        freq_max=0.368e9,
    )

    channel_1 = Channels(
        channels_id="vis_channels",
        spectral_windows=[spectral_window_1, spectral_window_2, spectral_window_3],
    )
    channel_2 = Channels(
        channels_id="pulsar_channels", spectral_windows=[spectral_window_4]
    )

    channels = [channel_1, channel_2]

    build_eb = functools.partial(
        ExecutionBlock,
        eb_id="eb-mvp01-20200325-00001",
        max_length=100,
        context=dict(foo="bar", baz=123),
        beams=beams,
        scan_types=scan_types,
        channels=channels,
        polarisations=[
            Polarisation(polarisations_id="all", corr_type=["XX", "XY", "YY", "YX"])
        ],
    )
    return build_eb(**overrides)


def holography_execution_block() -> ExecutionBlock:
    """
    Creates a test Execution Block for use in tests.
    """

    beam_mapping = BeamMapping(
        beam_ref="vis0",
        channels_ref="vis_channels",
        field_ref="Hercules A",
        polarisations_ref="all",
    )

    scan_types = [ScanType(scan_type_id=".default", beams=[beam_mapping])]

    spectral_window_1 = SpectralWindow(
        spectral_window_id="fsp_1_channels",
        count=14880,
        start=0,
        freq_min=0.35e9,
        freq_max=0.368e9,
        link_map=[(0, 0), (200, 1), (744, 2), (944, 3)],
    )

    channels = [
        Channels(
            channels_id="vis_channels",
            spectral_windows=[spectral_window_1],
        )
    ]

    eb = ExecutionBlockFactory(
        beams=[Beam(beam_id="vis0", function=BeamFunction.VISIBILITIES)],
        scan_types=scan_types,
        channels=channels,
    )

    return eb


VALID_EXECUTION_BLOCK_JSON = {
    "eb_id": "eb-mvp01-20200325-00001",
    "max_length": 100.0,
    "context": {"foo": "bar", "baz": 123},
    "beams": [
        {"beam_id": "vis0", "function": "visibilities"},
        {"beam_id": "pst1", "timing_beam_id": 1, "function": "pulsar timing"},
        {"beam_id": "pst2", "timing_beam_id": 2, "function": "pulsar timing"},
    ],
    "scan_types": [
        {
            "scan_type_id": ".default",
            "beams": [
                {
                    "beam_ref": "vis0",
                    "channels_ref": "vis_channels",
                    "polarisations_ref": "all",
                },
                {
                    "beam_ref": "pst1",
                    "field_ref": "M83",
                    "channels_ref": "pulsar_channels",
                    "polarisations_ref": "all",
                },
                {
                    "beam_ref": "pst2",
                    "field_ref": "Polaris Australis",
                    "channels_ref": "pulsar_channels",
                    "polarisations_ref": "all",
                },
            ],
        },
        {
            "scan_type_id": ".default",
            "derive_from": ".default",
            "beams": [{"field_ref": "M83", "beam_ref": "vis0"}],
        },
    ],
    "channels": [
        {
            "channels_id": "vis_channels",
            "spectral_windows": [
                {
                    "spectral_window_id": "fsp_1_channels",
                    "count": 744,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 350000000.0,
                    "freq_max": 368000000.0,
                    "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                },
                {
                    "spectral_window_id": "fsp_2_channels",
                    "count": 744,
                    "start": 2000,
                    "stride": 1,
                    "freq_min": 360000000.0,
                    "freq_max": 368000000.0,
                    "link_map": [[2000, 4], [2200, 5]],
                },
                {
                    "spectral_window_id": "zoom_window_1",
                    "count": 744,
                    "start": 4000,
                    "stride": 1,
                    "freq_min": 360000000.0,
                    "freq_max": 361000000.0,
                    "link_map": [[4000, 6], [4200, 7]],
                },
            ],
        },
        {
            "channels_id": "pulsar_channels",
            "spectral_windows": [
                {
                    "start": 0,
                    "freq_min": 350000000.0,
                    "freq_max": 368000000.0,
                    "count": 744,
                    "spectral_window_id": "pulsar_fsp_channels",
                }
            ],
        },
    ],
    "polarisations": [
        {"polarisations_id": "all", "corr_type": ["XX", "XY", "YY", "YX"]}
    ],
}

VALID_HOLOGRAPH_EXECUTION_BLOCK_JSON = {
    "eb_id": "eb-mvp01-20200325-00001",
    "max_length": 100.0,
    "context": {"foo": "bar", "baz": 123},
    "beams": [
        {"beam_id": "vis0", "function": "visibilities"},
    ],
    "scan_types": [
        {
            "scan_type_id": ".default",
            "beams": [
                {
                    "beam_ref": "vis0",
                    "channels_ref": "vis_channels",
                    "field_ref": "Hercules A",
                    "polarisations_ref": "all",
                },
            ],
        },
    ],
    "channels": [
        {
            "channels_id": "vis_channels",
            "spectral_windows": [
                {
                    "spectral_window_id": "fsp_1_channels",
                    "count": 14880,
                    "start": 0,
                    "freq_min": 350000000.0,
                    "freq_max": 368000000.0,
                    "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                },
            ],
        },
    ],
    "polarisations": [
        {"polarisations_id": "all", "corr_type": ["XX", "XY", "YY", "YX"]}
    ],
}

TEST_CASES = (
    pytest.param(
        json.dumps(VALID_EXECUTION_BLOCK_JSON),
        ExecutionBlockFactory,
        id=lbl("execution_block"),
    ),
    pytest.param(
        json.dumps(VALID_HOLOGRAPH_EXECUTION_BLOCK_JSON),
        holography_execution_block,
        id=lbl("execution_block_holograph"),
    ),
)
