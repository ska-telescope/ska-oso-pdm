"""
Tests for the SDP Polarisation schema to validate the
conversion between the JSON and Python representations
of the polarisations section of an SDP configuration
"""

import json

import pytest

from ska_oso_pdm.sb_definition.sdp.polarisation import Polarisation
from tests.unit.ska_oso_pdm.utils import lbl

VALID_POLARISATION_INSTANCE_1 = Polarisation(
    polarisations_id="ABC-123", corr_type=["XX", "YY"]
)
VALID_POLARISATION_JSON_DICT_1 = {
    "polarisations_id": "ABC-123",
    "corr_type": ["XX", "YY"],
}

VALID_POLARISATION_INSTANCE_2 = Polarisation(polarisations_id="XYZ-456", corr_type=[])
VALID_POLARISATION_JSON_DICT_2 = {"polarisations_id": "XYZ-456", "corr_type": []}

TEST_CASES = (
    pytest.param(
        json.dumps(VALID_POLARISATION_JSON_DICT_1),
        VALID_POLARISATION_INSTANCE_1.model_copy,
        id=lbl("polarisation1"),
    ),
    pytest.param(
        json.dumps(VALID_POLARISATION_JSON_DICT_2),
        VALID_POLARISATION_INSTANCE_2.model_copy,
        id=lbl("polarisation2"),
    ),
)
