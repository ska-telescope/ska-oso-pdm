"""
tests for the sdp_configuration schema to validate the
conversion between the JSON and Python representations
of the SDP configuration for an SKA Scheduling Block
"""

import json
from functools import partial

import pytest

from ska_oso_pdm.sb_definition.sdp import (
    PbDependency,
    ProcessingBlock,
    Resources,
    Script,
    ScriptKind,
    SDPConfiguration,
)
from tests.unit.ska_oso_pdm.utils import lbl

from .execution_block import ExecutionBlockFactory, holography_execution_block

PB_ID_TEMPL = "pb-mvp01-20200325-{n:05}"
SBI_ID_TEMPL = "sbi-mvp01-20200325-{n:05}"

SCRIPTS = (
    Script(name="vis_receive", kind=ScriptKind.REALTIME, version="0.1.0"),
    Script(name="test_realtime", kind=ScriptKind.REALTIME, version="0.1.0"),
    Script(name="ical", kind=ScriptKind.BATCH, version="0.1.0"),
    Script(name="dpreb", kind=ScriptKind.BATCH, version="0.1.0"),
)

DEPENDENCIES = (
    PbDependency(pb_ref=PB_ID_TEMPL.format(n=1), kind=["visibilities"]),
    PbDependency(pb_ref=PB_ID_TEMPL.format(n=3), kind=["calibration"]),
)


PROCESSING_BLOCKS = (
    ProcessingBlock(
        pb_id=PB_ID_TEMPL.format(n=1),
        script=SCRIPTS[0],
        sbi_refs=[SBI_ID_TEMPL.format(n=1)],
    ),
    ProcessingBlock(
        pb_id=PB_ID_TEMPL.format(n=2),
        script=SCRIPTS[1],
        sbi_refs=[SBI_ID_TEMPL.format(n=1)],
    ),
    ProcessingBlock(
        pb_id=PB_ID_TEMPL.format(n=3),
        script=SCRIPTS[2],
        sbi_refs=[SBI_ID_TEMPL.format(n=1)],
        dependencies=[DEPENDENCIES[0]],
    ),
    ProcessingBlock(
        pb_id=PB_ID_TEMPL.format(n=4),
        script=SCRIPTS[3],
        sbi_refs=[SBI_ID_TEMPL.format(n=1)],
        dependencies=[DEPENDENCIES[1]],
    ),
)

RECEPTORS_JSON = [
    "FS4",
    "FS8",
    "FS16",
    "FS17",
    "FS22",
    "FS23",
    "FS30",
    "FS31",
    "FS32",
    "FS33",
    "FS36",
    "FS52",
    "FS56",
    "FS57",
    "FS59",
    "FS62",
    "FS66",
    "FS69",
    "FS70",
    "FS72",
    "FS73",
    "FS78",
    "FS80",
    "FS88",
    "FS89",
    "FS90",
    "FS91",
    "FS98",
    "FS108",
    "FS111",
    "FS132",
    "FS144",
    "FS146",
    "FS158",
    "FS165",
    "FS167",
    "FS176",
    "FS183",
    "FS193",
    "FS200",
    "FS345",
    "FS346",
    "FS347",
    "FS348",
    "FS349",
    "FS350",
    "FS351",
    "FS352",
    "FS353",
    "FS354",
    "FS355",
    "FS356",
    "FS429",
    "FS430",
    "FS431",
    "FS432",
    "FS433",
    "FS434",
    "FS465",
    "FS466",
    "FS467",
    "FS468",
    "FS469",
    "FS470",
]

PROCESSING_BLOCKS_JSON = [
    {
        "pb_id": "pb-mvp01-20200325-00001",
        "sbi_refs": ["sbi-mvp01-20200325-00001"],
        "script": {"version": "0.1.0", "name": "vis_receive", "kind": "realtime"},
    },
    {
        "pb_id": "pb-mvp01-20200325-00002",
        "sbi_refs": ["sbi-mvp01-20200325-00001"],
        "script": {"version": "0.1.0", "name": "test_realtime", "kind": "realtime"},
    },
    {
        "pb_id": "pb-mvp01-20200325-00003",
        "sbi_refs": ["sbi-mvp01-20200325-00001"],
        "script": {"version": "0.1.0", "name": "ical", "kind": "batch"},
        "dependencies": [
            {"pb_ref": "pb-mvp01-20200325-00001", "kind": ["visibilities"]}
        ],
    },
    {
        "pb_id": "pb-mvp01-20200325-00004",
        "sbi_refs": ["sbi-mvp01-20200325-00001"],
        "script": {"version": "0.1.0", "name": "dpreb", "kind": "batch"},
        "dependencies": [
            {"pb_ref": "pb-mvp01-20200325-00003", "kind": ["calibration"]}
        ],
    },
]

ResourcesFactory = partial(
    Resources,
    csp_links=[1, 2, 3, 4],
    receptors=RECEPTORS_JSON,
    receive_nodes=10,
)

SDPConfigurationFactory = partial(
    SDPConfiguration,
    resources=ResourcesFactory(),
    execution_block=ExecutionBlockFactory(),
    processing_blocks=PROCESSING_BLOCKS,
)


def holography_sdp_configuration():
    sdp_config = SDPConfigurationFactory(
        execution_block=holography_execution_block(),
        resources=ResourcesFactory(receptors=["SKA001", "SKA036", "SKA063", "SKA100"]),
    )

    return sdp_config


VALID_SDP_CONFIG_JSON = {
    "execution_block": {
        "eb_id": "eb-mvp01-20200325-00001",
        "max_length": 100.0,
        "context": {"foo": "bar", "baz": 123},
        "beams": [
            {"beam_id": "vis0", "function": "visibilities"},
            {"beam_id": "pst1", "timing_beam_id": 1, "function": "pulsar timing"},
            {"beam_id": "pst2", "timing_beam_id": 2, "function": "pulsar timing"},
        ],
        "scan_types": [
            {
                "scan_type_id": ".default",
                "beams": [
                    {
                        "beam_ref": "vis0",
                        "channels_ref": "vis_channels",
                        "polarisations_ref": "all",
                    },
                    {
                        "beam_ref": "pst1",
                        "field_ref": "M83",
                        "channels_ref": "pulsar_channels",
                        "polarisations_ref": "all",
                    },
                    {
                        "beam_ref": "pst2",
                        "field_ref": "Polaris Australis",
                        "channels_ref": "pulsar_channels",
                        "polarisations_ref": "all",
                    },
                ],
            },
            {
                "scan_type_id": ".default",
                "derive_from": ".default",
                "beams": [{"beam_ref": "vis0", "field_ref": "M83"}],
            },
        ],
        "channels": [
            {
                "channels_id": "vis_channels",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 744,
                        "start": 0,
                        "stride": 2,
                        "freq_min": 350000000,
                        "freq_max": 368000000,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                    {
                        "spectral_window_id": "fsp_2_channels",
                        "count": 744,
                        "start": 2000,
                        "stride": 1,
                        "freq_min": 360000000,
                        "freq_max": 368000000,
                        "link_map": [[2000, 4], [2200, 5]],
                    },
                    {
                        "spectral_window_id": "zoom_window_1",
                        "count": 744,
                        "start": 4000,
                        "stride": 1,
                        "freq_min": 360000000,
                        "freq_max": 361000000,
                        "link_map": [[4000, 6], [4200, 7]],
                    },
                ],
            },
            {
                "channels_id": "pulsar_channels",
                "spectral_windows": [
                    {
                        "spectral_window_id": "pulsar_fsp_channels",
                        "count": 744,
                        "start": 0,
                        "freq_min": 350000000,
                        "freq_max": 368000000,
                    }
                ],
            },
        ],
        "polarisations": [
            {"polarisations_id": "all", "corr_type": ["XX", "XY", "YY", "YX"]}
        ],
    },
    "processing_blocks": PROCESSING_BLOCKS_JSON,
    "resources": {
        "csp_links": [1, 2, 3, 4],
        "receptors": RECEPTORS_JSON,
        "receive_nodes": 10,
    },
}

VALID_HOLOGRAPH_SDP_CONFIG_JSON = {
    "execution_block": {
        "eb_id": "eb-mvp01-20200325-00001",
        "max_length": 100.0,
        "context": {"foo": "bar", "baz": 123},
        "beams": [
            {"beam_id": "vis0", "function": "visibilities"},
        ],
        "scan_types": [
            {
                "scan_type_id": ".default",
                "beams": [
                    {
                        "beam_ref": "vis0",
                        "channels_ref": "vis_channels",
                        "field_ref": "Hercules A",
                        "polarisations_ref": "all",
                    },
                ],
            },
        ],
        "channels": [
            {
                "channels_id": "vis_channels",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 14880,
                        "start": 0,
                        "freq_min": 350000000.0,
                        "freq_max": 368000000.0,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                ],
            },
        ],
        "polarisations": [
            {"polarisations_id": "all", "corr_type": ["XX", "XY", "YY", "YX"]}
        ],
    },
    "processing_blocks": PROCESSING_BLOCKS_JSON,
    "resources": {
        "csp_links": [1, 2, 3, 4],
        "receptors": ["SKA001", "SKA036", "SKA063", "SKA100"],
        "receive_nodes": 10,
    },
}

TEST_CASES = (
    pytest.param(
        json.dumps(VALID_SDP_CONFIG_JSON),
        SDPConfigurationFactory,
        id=lbl("sdp_configuration"),
    ),
    pytest.param(
        json.dumps(VALID_HOLOGRAPH_SDP_CONFIG_JSON),
        holography_sdp_configuration,
        id=lbl("sdp_configuration_holography"),
    ),
)
