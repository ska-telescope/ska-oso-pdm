"""
tests for the SBD, SBI, EB, Project status history to validate the
conversion between the JSON and Python representations
"""

from datetime import datetime, timezone

import pytest

from ska_oso_pdm import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)
from ska_oso_pdm._shared import Metadata
from tests.unit.ska_oso_pdm.utils import lbl

SAMPLE_DATETIME = datetime(1988, 12, 22, tzinfo=timezone.utc)

STATUS_HISTORY_JSON = """
    "metadata": {
        "version": 1,
        "created_by": "TestUser",
        "created_on": "1988-12-22T00:00:00Z",
        "last_modified_by": "TestUser",
        "last_modified_on": "1988-12-22T00:00:00Z",
        "pdm_version": "15.3.0"
    }
"""
SBD_STATUS_HISTORY_JSON = (
    """
{
    "sbd_ref": "sbd-mvp01-20200325-00001",
    "current_status":"Draft",
    "previous_status":"Draft",
    "sbd_version":1,
    """
    + STATUS_HISTORY_JSON
    + """
}"""
)

SBI_STATUS_HISTORY_JSON = (
    """
{
    "sbi_ref": "sbi-mvp01-20200325-00001",
    "current_status":"Created",
    "previous_status":"Created",
    "sbi_version":1,
    """
    + STATUS_HISTORY_JSON
    + """
}"""
)

EB_STATUS_HISTORY_JSON = (
    """
{
    "eb_ref": "eb-mvp01-20200325-00001",
    "current_status":"Created",
    "previous_status":"Created",
    "eb_version":1,
    """
    + STATUS_HISTORY_JSON
    + """
}"""
)

PROJECT_STATUS_HISTORY_JSON = (
    """
{
    "prj_ref": "eb-mvp01-20200325-00001",
    "current_status":"Draft",
    "previous_status":"Draft",
    "prj_version":1,
    """
    + STATUS_HISTORY_JSON
    + """
}"""
)


def default_metadata() -> Metadata:
    return Metadata(
        version=1,
        created_by="TestUser",
        created_on=SAMPLE_DATETIME,
        last_modified_by="TestUser",
        last_modified_on=SAMPLE_DATETIME,
        pdm_version="15.3.0",
    )


SBD_STATUS_HISTORY_OBJECT = SBDStatusHistory(
    sbd_ref="sbd-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Draft",
    previous_status="Draft",
    sbd_version=1,
)

SBI_STATUS_HISTORY_OBJECT = SBIStatusHistory(
    sbi_ref="sbi-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Created",
    previous_status="Created",
    sbi_version=1,
)

EB_STATUS_HISTORY_OBJECT = OSOEBStatusHistory(
    eb_ref="eb-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Created",
    previous_status="Created",
    eb_version=1,
)

PROJECT_STATUS_HISTORY_OBJECT = ProjectStatusHistory(
    prj_ref="eb-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Draft",
    previous_status="Draft",
    prj_version=1,
)

TEST_CASES = (
    pytest.param(
        SBD_STATUS_HISTORY_JSON,
        SBD_STATUS_HISTORY_OBJECT.model_copy,
        id=lbl("sbd_status"),
    ),
    pytest.param(
        SBI_STATUS_HISTORY_JSON,
        SBI_STATUS_HISTORY_OBJECT.model_copy,
        id=lbl("sbi_status"),
    ),
    pytest.param(
        EB_STATUS_HISTORY_JSON, EB_STATUS_HISTORY_OBJECT.model_copy, id=lbl("eb_status")
    ),
    pytest.param(
        PROJECT_STATUS_HISTORY_JSON,
        PROJECT_STATUS_HISTORY_OBJECT.model_copy,
        id=lbl("prj_status"),
    ),
)
