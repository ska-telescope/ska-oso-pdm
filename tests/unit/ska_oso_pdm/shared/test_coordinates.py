import warnings
from functools import partial

import pytest

from ska_oso_pdm._shared import (
    AltAzCoordinates,
    EquatorialCoordinates,
    EquatorialCoordinatesPST,
    HorizontalCoordinates,
    ICRSCoordinates,
    SolarSystemObject,
    SolarSystemObjectName,
    SpecialCoordinates,
    Target,
)

# Backwards-compatibility tests...
eq_coord = partial(EquatorialCoordinates)
eq_pst_coord = partial(
    EquatorialCoordinatesPST,
    target_id="test target",
    ra_str="04:01:18.4",
    dec_str="-84:56:35.9",
)
hz_coord = partial(HorizontalCoordinates, az=45, el=10)
icrs_coord = partial(ICRSCoordinates, ra_str="04:01:18.4", dec_str="-84:56:35.9")
altaz_coord = partial(AltAzCoordinates, az=45, el=10)
sso_coord = partial(SolarSystemObject, name="Sun")
special_coord = partial(SpecialCoordinates, name="Sun")


ISINSTANCE_CASES = (
    (eq_coord, EquatorialCoordinates, True),
    (icrs_coord, ICRSCoordinates, True),
    (icrs_coord, EquatorialCoordinates, False),
    (icrs_coord, HorizontalCoordinates, False),
    (icrs_coord, AltAzCoordinates, False),
    (hz_coord, HorizontalCoordinates, True),
    (altaz_coord, HorizontalCoordinates, True),
    (altaz_coord, ICRSCoordinates, False),
    (altaz_coord, SpecialCoordinates, False),
    (sso_coord, SolarSystemObject, True),
    (special_coord, SolarSystemObject, True),
    (special_coord, ICRSCoordinates, False),
    (special_coord, AltAzCoordinates, False),
    (eq_pst_coord, EquatorialCoordinatesPST, True),
    (eq_pst_coord, ICRSCoordinates, False),
)


@pytest.mark.parametrize(("get_instance", "klass", "expected"), ISINSTANCE_CASES)
def test_isinstance_backwards_compatibility(get_instance, klass, expected):
    """SpecialCoordinates and AltAzCoordinates classes should pass an isinstance check
    for the deprecated SolarSystemObject and HorizontalCoordinates classes."""
    assert isinstance(get_instance(), klass) is expected


@pytest.mark.parametrize("get_instance", (hz_coord, sso_coord, eq_coord, eq_pst_coord))
def test_deprecation_warning(get_instance):
    with pytest.deprecated_call():
        get_instance()


@pytest.mark.parametrize(
    "get_instance",
    (
        icrs_coord,
        altaz_coord,
        special_coord,
    ),
)
def test_no_warning_triggered(get_instance):
    with warnings.catch_warnings():
        # From pytest docs, this filter would convert
        # convert warnings to errors and fail the test
        # https://docs.pytest.org/en/7.0.x/how-to/capture-warnings.html#additional-use-cases-of-warnings-in-tests
        warnings.simplefilter("error")
        get_instance()


@pytest.mark.parametrize(
    ("make_target", "expected"),
    (
        (
            partial(
                Target,
                reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.MARS),
            ),
            SpecialCoordinates(name=SolarSystemObjectName.MARS),
        ),
        (
            partial(
                Target,
                reference_coordinate={
                    "kind": "sso",
                    "reference_frame": "special",
                    "name": "Mars",
                },
            ),
            SpecialCoordinates(name=SolarSystemObjectName.MARS),
        ),
        (
            partial(
                Target,
                reference_coordinate=HorizontalCoordinates(az=0, el=0),
            ),
            AltAzCoordinates(az=0, el=0),
        ),
        (
            partial(
                Target,
                reference_coordinate={
                    "kind": "horizontal",
                    "az": 0,
                    "el": 0,
                },
            ),
            AltAzCoordinates(az=0, el=0),
        ),
    ),
)
def test_load_old_formats(make_target, expected):
    result = make_target()
    assert result.reference_coordinate == expected
