import pytest

from ska_oso_pdm._shared.ids import SKUID_REGEX


class TestSKUIDRegEx:
    @pytest.mark.parametrize(
        "skuid_id",
        (
            "sbd-t0001-20240417-00002",
            "sbi-t0001-20221215-00333",
            "eb-t0003-20090101-16392",
            "opp-t0001-20240303-00002",
            "opj-t0001-20221201-00100",
            "pb-t0001-20221215-00013",
        ),
    )
    def test_skuid_regex_valid_id(self, skuid_id):
        assert SKUID_REGEX.match(skuid_id)

    @pytest.mark.parametrize(
        "skuid_id",
        (
            "sb-t0001-20240417-00002",  # incorrect entity type
            "sbi--20221215-00033",  # incorrect generator instance
            "eb-t0003-200912011-16392",  # incorrect date
            "opp-t0001-20240303-0000b",  # incorrect sequence number
        ),
    )
    def test_skuid_regex_invalid_id(self, skuid_id):
        assert not SKUID_REGEX.match(skuid_id)
