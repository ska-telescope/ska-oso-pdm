"""
Unit tests for to verify object equality of SBD, SBI, EB, Project status history objects.
"""

from datetime import datetime, timezone

import pytest

from ska_oso_pdm import (
    OSOEBStatusHistory,
    ProjectStatusHistory,
    SBDStatusHistory,
    SBIStatusHistory,
)
from ska_oso_pdm._shared import Metadata

SAMPLE_DATETIME = datetime(1988, 12, 22, tzinfo=timezone.utc)


def default_metadata() -> Metadata:
    return Metadata(
        version=1,
        created_by="TestUser",
        created_on=SAMPLE_DATETIME,
        last_modified_by="TestUser",
        last_modified_on=SAMPLE_DATETIME,
    )


sbd_status_history = SBDStatusHistory(
    sbd_ref="sbd-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Draft",
    previous_status="Draft",
    sbd_version=1,
)

sbd_status_history1 = SBDStatusHistory(
    sbd_ref="sbd-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Draft",
    previous_status="Draft",
    sbd_version=1,
)

sbi_status_history = SBIStatusHistory(
    sbi_ref="sbi-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Created",
    previous_status="Created",
    sbi_version=1,
)

sbi_status_history1 = SBIStatusHistory(
    sbi_ref="sbi-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Created",
    previous_status="Created",
    sbi_version=1,
)

eb_status_history = OSOEBStatusHistory(
    eb_ref="eb-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Created",
    previous_status="Created",
    eb_version=1,
)

eb_status_history1 = OSOEBStatusHistory(
    eb_ref="eb-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Created",
    previous_status="Created",
    eb_version=1,
)

prj_status_history = ProjectStatusHistory(
    prj_ref="prj-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Draft",
    previous_status="Draft",
    prj_version=1,
)
prj_status_history1 = ProjectStatusHistory(
    prj_ref="prj-mvp01-20200325-00001",
    metadata=default_metadata(),
    current_status="Draft",
    previous_status="Draft",
    prj_version=1,
)


@pytest.mark.parametrize(
    "entity_object1, entity_object2",
    [
        (sbd_status_history, sbd_status_history1),
        (sbi_status_history, sbi_status_history1),
        (eb_status_history, eb_status_history1),
        (prj_status_history, prj_status_history1),
    ],
)
def test_to_check_equality_of_entity_objects(entity_object1, entity_object2):
    """
    Verifies SBD, SBI, EB, PRJ status history objects equality.
    """
    assert entity_object1 == entity_object2
    entity_object1.metadata.version = 2
    assert entity_object1 != entity_object2


@pytest.mark.parametrize(
    "entity_object1",
    [
        (sbd_status_history),
        (sbi_status_history),
        (eb_status_history),
        (prj_status_history),
    ],
)
def test_to_check_invalid_current_status(entity_object1):
    """
    Verifies ValueError raised SBD, SBI, EB, PRJ status history objects when those are not equal.
    """

    with pytest.raises(ValueError):
        entity_object1.current_status = "test-created"
