import json
from contextlib import nullcontext as does_not_raise

import pytest
from pytest import FixtureRequest

from tests.unit.ska_oso_pdm.utils import GetEntity, assert_json_is_equal

from .serialisation_cases import TEST_CASES


@pytest.mark.freeze_time("2022-03-28T15:43:53.971548")
@pytest.mark.parametrize(("reference_json", "get_entity"), TEST_CASES)
class TestLoadAndDump:
    """
    Standardized class to verify de/serialisation from/to JSON.

    These parameterised tests verify that for any PdmEntity:
    - instances are serialised to JSON correctly
    - serialisation does not modify the original object
    - JSON is deserialised to instances correctly
    - unexpected fields are ignored
    """

    def test_round_trip(
        self,
        reference_json: str,  # pylint: disable=unused-argument
        get_entity: GetEntity,
    ):
        """
        Verify that we can round-trip dump to JSON and load back to
        an equivalent PDM entity.
        """
        entity = get_entity()
        klass = entity.__class__
        serialised = entity.model_dump_json()
        loaded = klass.model_validate_json(serialised)
        assert loaded == entity

    def test_dump(
        self, reference_json: str, get_entity: GetEntity, request: FixtureRequest
    ):
        """
        Verify that instances are marshalled to JSON correctly.

        Expected behaviour is that an instance of cls created with
        constructor_args serialises to the expected_json string.

        Access to the pytest FixtureRequest is required to gain
        access to the Pytest marks set in the test context.
        """
        entity = get_entity()
        serialised = entity.model_dump_json()

        for mark in request.node.own_markers:
            if mark.name == "exported_json_should_differ":
                expectation = pytest.raises(AssertionError)
                break
        else:
            expectation = does_not_raise()

        with expectation:
            assert_json_is_equal(serialised, reference_json)

    def test_dump_does_not_mutate(
        self,
        reference_json: str,  # pylint: disable=unused-argument
        get_entity: GetEntity,
    ):
        # Ensures that serialising does not mutate any fields on the original object
        entity = get_entity()
        entity.model_dump_json()
        assert get_entity() == entity

    def test_load(self, reference_json: str, get_entity: GetEntity):
        """
        Verify that instances are unmarshalled from JSON correctly.

        The behaviour considered correct is that input_json unmarshalled to an
        instance of cls results in an instance that compares equal to one created
        with constructor_args.
        """
        entity = get_entity()
        klass = entity.__class__
        deserialised = klass.model_validate_json(reference_json)
        assert entity == deserialised

    def test_load_extra_fields_safely(self, reference_json: str, get_entity: GetEntity):
        # Forward-compatibility relies on accepting new fields.
        # Ensure that the schema uses relaxed validation by adding an extra field
        # to the input JSON and checking the resulting object is still the same.
        entity = get_entity()
        klass = entity.__class__
        appended_json = json.loads(reference_json)
        appended_json.update({"test_extra_attribute_key": "test_extra_attribute_value"})
        deserialised = klass.model_validate_json(json.dumps(appended_json))
        assert entity == deserialised
