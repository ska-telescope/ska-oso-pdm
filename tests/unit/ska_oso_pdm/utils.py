"""
Utilities for PDM entity serialisation tests.
"""

import inspect
import json
from os import PathLike
from pathlib import Path
from pprint import pformat
from typing import Callable

from deepdiff import DeepDiff

from ska_oso_pdm._shared import PdmObject

GetEntity = Callable[[], PdmObject]

TEST_ID_SUFFIX = 12345
TEST_TARGET_ID = f"target-{TEST_ID_SUFFIX}"
TEST_CSP_CONFIGURATION_ID = f"csp-configuration-{TEST_ID_SUFFIX}"
TEST_SCAN_DEFINITION_ID = f"scan-definition-{TEST_ID_SUFFIX}"


def assert_json_is_equal(json_a, json_b):
    """
    Utility function to compare two JSON objects
    """
    # key/values in the generated JSON do not necessarily have the same order
    # as the test string, even though they are equivalent JSON objects, e.g.,
    # subarray_id could be defined after dish. Ensure a stable test by
    # comparing the JSON objects themselves.
    obj_a = json.loads(json_a)
    obj_b = json.loads(json_b)
    try:
        assert obj_a == obj_b
    except AssertionError as exc:
        # raise a more useful exception that shows *where* the JSON differs
        diff = DeepDiff(obj_a, obj_b, ignore_order=True)
        if diff:  # Could be a spurious failure from ordering in a set.
            raise AssertionError(f"JSON not equal:\n{pformat(diff)}") from exc


def add_extra_attribute_to_json(valid_json):
    """
    Created method to add extra test attribute in original JSON
    to validate relax validation
    :param vali_json: Original JSON to add extra attributes
    """
    json_loads = json.loads(valid_json)
    json_loads.update({"test_extra_attribute_key": "test_extra_attribute_value"})
    return json.dumps(json_loads)


def lbl(test_id: str) -> str:
    """
    Helper function to label test IDs with the file name where
    that test case was defined.

    Most serialisation test failures are from errors in the
    fixture data, not errors in the test code itself. Labeling
    these test cases with the filename makes it easier
    for devs to go directly to where the problem is.
    """
    project_root = Path(__file__).parents[3]
    filepath = Path(inspect.stack()[1].filename).relative_to(project_root)
    return f"{filepath}:{test_id}"


def load_string_from_file(filepath: PathLike) -> str:
    """
    Return the content of a file as a string (UTF-8 assumed)
    path is relative to the call site of this utility.
    """
    calldir = Path(inspect.stack()[1].filename).parent
    with open(calldir / filepath, "r", encoding="utf-8") as fh:
        return fh.read()
